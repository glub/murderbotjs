import { Message } from "discord.js";
import { manager } from './nlp-manager';

export async function passive (message: Message) {
    try {
        const mentions = message.mentions;
        if (message.guildId && mentions?.users?.size && !mentions.roles?.size && !mentions.everyone) {
            const userFrom = message.author.id;
            const mentions = Array.from(message.mentions.users.values()).map(user => `'${user.id}'`);
            const query = message.content;
            const response = await manager.process(query);
            if (response.sentiment.vote === 'neutral') {
                return;
            }
            let sentiment = response.sentiment.vote === 'positive' ? 1 : -1;
            const scores = await this.allQuery(`
            select
                userTo, 
                sentiment
            from 
                sentiment 
            where 
                userFrom = '${userFrom}'
                and guild = '${message.guildId}'
                and userTo in (${mentions.join(",")})`)
            for (let i =0, N=mentions.length; i < N; i++) {
                const userTo = mentions[i];
                const existing = scores.find(score => `'${score.userTo}'` === userTo);
                let value = sentiment;
                if (existing) {
                    value = (value + existing.sentiment) / 2;
                }
                const query = `insert or replace into sentiment values (${userFrom}, ${userTo},'${message.guildId}',${value})`;
                console.log(existing, query);
                this.runQuery(query);
            }
        }

    } catch (e) {
        console.error(e);
    }

};