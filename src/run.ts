import { Murderbot } from './murderbot';
const keys = require('../keys.js');
const {db}= process.argv.slice(2).reduce((result, element) => {
        const [key, val] = element.split('=');
        result[key] = val;
        return result;
 }, {}) as {db: string};
let mb = new Murderbot(keys.MURDERBOT, db);
