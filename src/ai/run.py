import tensorflow as tf
import sys
name = sys.argv[1]
txt = sys.argv[2]
size = sys.argv[3] or 200
one_step_model = tf.saved_model.load(name)
states = None
next_char = tf.constant([txt])
result = [next_char]

for n in range(size):
  next_char, states = one_step_model.generate_one_step(next_char, states=states)
  result.append(next_char)

result = tf.strings.join(result)
print(result[0].numpy().decode('utf-8'), '\n\n' + '_'*80)
sys.stdout.flush()