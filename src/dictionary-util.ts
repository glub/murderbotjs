import { promisify } from 'util';
const request = promisify(require('request'));


export async function antonyms(word: string): Promise<string[]> {
        return words('rel_ant=' + word);
    };

export async function synonyms(word: string): Promise<string[]> {
        return words('rel_syn=' + word);
    };
export async function associated(word: string): Promise<string[]> {
        return words('rel_trg=' + word);
    };

export async function kindsOf(word: string): Promise<string[]> {
        return words('rel_spc=' + word);
    };
export async function generic(word: string): Promise<string[]> {
        return words('rel_gen=' + word);
    };
export async function rhyme(word: string): Promise<string[]> {
        return words('rel_rhy=' + word);
    };
export async function soundLike(word: string): Promise<string[]> {
        return words('rel_hom=' + word);
    };


    export async function words(query: string): Promise<string[]> {
        const response = await request({
            url: 'https://api.datamuse.com/words?' + query
        });
        const result = JSON.parse(response.body);
        return result.map((e: {word: string}) => e.word);
    }