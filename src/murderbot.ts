import { resolve } from 'path';
import { promisify } from 'util';
import { readdir } from 'fs';
import { passive } from './passive';
const request = promisify(require('request'));
const readdirAsync = promisify(readdir);
export const DEFAULT_PRIORITY: number = 50;
export const CPU_USAGE_ENTRIES: number = 100;
export const CPU_USAGE_INTERVAL: number = 60000;
const util = require('./util');
import {
    Message,
    MessageReaction,
    Client,
    User,
    PartialMessageReaction,
    Partials,
    GatewayIntentBits,
    IntentsBitField,
    SlashCommandBuilder,
    Interaction,
    REST,
    Routes,
    MessageComponentInteraction,
    RESTPostAPIChatInputApplicationCommandsJSONBody,
    ChatInputCommandInteraction,
    AutocompleteInteraction,
    
} from 'discord.js';
export interface Command {
    name: string;
    help?: string;

    priority?: number;
    admin?: boolean;

    regex?: RegExp;
    respond?: (message: Message, match?: RegExpMatchArray) => Promise<void>;
   
    reactIf?: (message: MessageReaction | PartialMessageReaction, user?: User) => Promise<boolean>;
    react?: (message: MessageReaction | PartialMessageReaction, user?: User) => Promise<void>;
    noType?: boolean;


    interact?: (interaction: MessageComponentInteraction) => Promise<void>;
    interactionId?: string;

    slashJSON?: RESTPostAPIChatInputApplicationCommandsJSONBody;
    slashRespond?: (interaction: Interaction) => Promise<void>;
    slashName?: string;
    slashAutocomplete?: (interaction: AutocompleteInteraction) => Promise<{name: string, value: any}[]>;
}


const INTENTS = [

    GatewayIntentBits.DirectMessageReactions,
    GatewayIntentBits.DirectMessageTyping,
    GatewayIntentBits.DirectMessages,
    GatewayIntentBits.GuildMessageReactions,
    GatewayIntentBits.GuildMessageTyping,

    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent,
    GatewayIntentBits.GuildMembers

];

// const INTENTS = new IntentsBitField();
// INTENTS.add(
//     IntentsBitField.Flags.DirectMessages,
//     IntentsBitField.Flags.MessageContent,
//     IntentsBitField.Flags.DirectMessageTyping,
//     IntentsBitField.Flags.DirectMessageReactions,
//     IntentsBitField.Flags.GuildMessageTyping,
//     IntentsBitField.Flags.GuildMessages,
//     IntentsBitField.Flags.GuildMessageReactions,
//     IntentsBitField.Flags.GuildMembers
// );

const PARTIALS = [
    Partials.Channel,
    Partials.Message,
    Partials.Reaction
];

const tables = [
'log (user TEXT, guild TEXT, operation TEXT, error TEXT)',
'settings (guild TEXT UNIQUE, nsfwFilter INTEGER)',
'locations (userID TEXT UNIQUE, city TEXT)',
'quiz_answers (guild TEXT, answer TEXT)',
'quiz_scores (userID TEXT, guild TEXT, points INTEGER, CONSTRAINT unq UNIQUE (userID, guild))',
'sentiment (userFrom TEXT, userTo TEXT, guild TEXT, strength REAL, count INTEGER, CONSTRAINT unq UNIQUE (userFrom, userTo, guild))',
'emojis (userID TEXT, guild TEXT, emoji TEXT, count INTEGER, CONSTRAINT unq UNIQUE (userID, guild, emoji))',
'comic_templates (image TEXT, name TEXT, bubbles TEXT, CONSTRAINT unq UNIQUE (name))',
'self_roles(guild TEXT, name TEXT, id TEXT, category TEXT)',
'self_role_posts(guild TEXT, channel TEXT, post TEXT)'
];




const clues = new (require('sqlite-async'))();
clues.open(resolve(__dirname, '../clues.sqlite3'));
const dnd = new (require('sqlite-async'))();
dnd.open(resolve(__dirname, '../dnd.db'));

export class Murderbot {
    datasets = {
        wikihow: require('../whow.json'),
        quiz: require('../jeopardy.json'),
        yiff: require('../yiff.json'),
        dnd,
        clues,
    }
    murdercall: RegExp = /^(mb|murderbot) +((.|\n)+)/im;
    signature: string = '';
    commands: Command[] = [];

    slashCommandsJSON: RESTPostAPIChatInputApplicationCommandsJSONBody[] = [];
    slashCommands: {[x: string]: (interaction: ChatInputCommandInteraction) => Promise<void>} = {};
    slashAutocomplete: {[x: string]: (interaction: AutocompleteInteraction) => Promise<{name: string, value: any}[]>} = {};

    appId: string = '222788458349854721';

    admin: Command[] = [];
    reactions: Command[] = [];

    interactions: {[x: string]: (i: MessageComponentInteraction) => Promise<void>} = {};

    cpuUsage: NodeJS.CpuUsage[] = [{system: 0, user: 0}];
    client: Client;
    help: string = "# help? ur beyond help\n# [] denotes an optional parameter";

    // db: any;

    adminID: string = '189676343972200449';

    constructor(private token: string, public db: string) {
        this.client = new Client({intents: INTENTS, partials: PARTIALS});
        this.start();
    }
    async start() {
        this.client.on('ready', () => {
            console.log('Logged the absolute FUCK in');
            this.sortCommands();
        });
        this.client.on('messageCreate', message => this.respondToMessage(message));
        this.client.on('messageReactionAdd', (messageReaction, user) => this.respondToReaction(messageReaction as MessageReaction, user as User));
        this.client.on('disconnect', () => this.retry());
        this.client.on('error', () => this.retry());
    

        this.client.on("interactionCreate", interaction => {
            // console.log(interaction);

            if (interaction.isMessageComponent()) {
                this.respondToComponentInteraction(interaction);
            }
            if (interaction.isChatInputCommand()) {
                this.respondToSlashCommand(interaction);
            }
            if (interaction.isAutocomplete()) {
                this.fetchAutocomplete(interaction);
            } 
        });

    
        let initUsage = process.cpuUsage();
        // await this.initDB();
        await this.loadModules();
        setInterval(() => {
            this.recordCpuUsage(initUsage);
            initUsage = process.cpuUsage();
            }, CPU_USAGE_INTERVAL);
        await this.client.login(this.token)
            .catch(e => console.error('Unable to start.', e));
        // await util.download(this.client.user.displayAvatarURL(), 'ava.png');
    }

    retry() {
        setTimeout(() => {
            this.client.login(this.token)
            .catch(e => console.error('Unable to start.', e));
        }, 5000);
    }

    sortCommands() {
        [this.commands, this.admin]
            .forEach(array => array.sort((a,b) => {           
                return a.priority - b.priority;
            })
        );
    }
    addCommand(command: Command): void {

        
        if (command.interactionId) {
            this.interactions[command.interactionId] = command.interact.bind(this);
        } else {
            if (command.slashJSON) {
                this.slashCommandsJSON.push(command.slashJSON);
                this.slashCommands[command.slashName] = command.slashRespond.bind(this);
            }
            if (command.slashAutocomplete) {
                this.slashAutocomplete[command.slashName] = command.slashAutocomplete.bind(this)
            }


            let where = this.commands;
            if (command.react) {
                where = this.reactions;
            } else if (command.admin) {
                where = this.admin;
            }
    
            command.respond && (command.respond = command.respond.bind(this));
            command.react && (command.react = command.react.bind(this));
            command.reactIf && (command.reactIf = command.reactIf.bind(this));
            command.priority = command.priority || DEFAULT_PRIORITY;
    
            if (command.help) {
                this.help += '\n\n' + command.help + '\n';
            }
    
            if (command.respond || command.react) {
                where.push(command);
            }
        }
        

        console.log('- '+command.name);
    }

    getHelpPages(query: string = ""): string[] {
        const entries = this.commands.filter(command => command.help?.indexOf(query) > -1).map(command => command.help);
        let pages = [""];
        entries.forEach(entry => {
            let i = pages.length - 1;
            if (pages[i].length + entry.length >= 1800 ) {
                console.log(pages.length);
                console.log(pages[i].length);
                console.log(pages[i]);
                pages.push("");
            } else{
                pages[i] = pages[i] + '\n' + entry;
            }
        }); 
        pages = pages.map(page => '```ldif\n' + page + '\n```');
        return pages;
    }

    async fetchAutocomplete(interaction: AutocompleteInteraction): Promise<void> {
        let command = this.slashAutocomplete[interaction.commandName];
        if (command) {
            let list = await command(interaction);
            interaction.respond(list);
        } else {
            interaction.respond([{
                name:  'no categories have been added yet',
                value: null
            }])
        }
    }

    async respondToComponentInteraction(interaction: MessageComponentInteraction): Promise<void> {
        const command = this.interactions[interaction.customId];
        if (command) {
            command(interaction);
        } else {
            // interaction.reply({
            //     content: 'no command registered for this interaction (skill issue)',
            //     ephemeral: true
            // });
            
        }
    }

    async respondToSlashCommand(interaction: ChatInputCommandInteraction): Promise<void> {
        const command = this.slashCommands[interaction.commandName];
        if (command) {
            const username = interaction.user.username;
            const guild = interaction.guild ? interaction.guild.name : 'DM ';
            const operation =interaction.commandName;
            console.log(username, guild, operation);
            try {
                await command(interaction);
            } catch(e) {
                console.log(e);
            }
        } else {
            interaction.reply({
                ephemeral: true,
                content: 'no command registered for this interaction (skill issue)'
            });
        }
    }


    async respondToMessage(message: Message): Promise<void> {
        // if (!message.channel.isDMBased()) {
        //     console.log(message)
        // }

        if(message.author.bot) {
            return;
        }
        const murdercalled = message.content.match(this.murdercall);
        if (!murdercalled) {
            passive.bind(this)(message);
            return;
        }
        const content = murdercalled[2];
        const where = this.commands.slice();
        if (message.author.id === this.adminID) {
            where.unshift(...this.admin);
        }
        for(let command of where) {
            const match = !!command.regex && content.match(command.regex);
            // console.log(command, match, content)
            if (match) {
                const username = message.author.username;
                const guild = message.guild ? message.guild.name : 'DM ';
                const operation = command.name;
                console.log(username, guild, operation);
                
                try {
                    if (!command.noType) {
                        await message.channel.sendTyping();
                    }
                    await command.respond(message, match);
                }catch(e) {
                    if(command.respond) {
                        await message.channel.send(':anger: ERROЯ: ' + e.message);
                    }
                    console.error(e);
                    
                }
                // await this.log(username, guild, operation, error);
                return;
            } 
        }
    }
    recordCpuUsage(previous?: NodeJS.CpuUsage): NodeJS.CpuUsage {
        const usage = process.cpuUsage(previous);
        const value = {
            user: (usage.system + usage.user) / 10 / CPU_USAGE_INTERVAL,
            system: usage.system / 10 / CPU_USAGE_INTERVAL
        };
        this.cpuUsage.push(value);
        if(this.cpuUsage.length > CPU_USAGE_ENTRIES) {
            this.cpuUsage.shift();
        }

        return usage;
    }
    async respondToReaction(reaction: MessageReaction | PartialMessageReaction, user: User): Promise<void> {

        let where = this.reactions;
        if (user.id !== this.adminID) {
            where = where.filter(c => !c.admin);
        }

        for(let command of where) {
            
            let triggered = await command.reactIf(reaction, user);
            if(triggered) {
                try {
                    console.log('executing ' + command.name);
                    command.react(reaction, user);
                }catch(e) {
                    console.error(e);
                }
            }
        }
    }
    async loadModules(): Promise<void> {
        const files: string[] = await readdirAsync(resolve(__dirname,'modules'));
        files 
            .forEach(file => {
                let commands: Command[] = [];
                try {
                    commands = require(resolve(__dirname, 'modules',file));
                } catch(e) {
                    console.error('Error while loading module: ' + file, e);
                }
                if(commands.length) {
                    console.log('Loaded module: ' + file);
                    commands.forEach(command => this.addCommand(command));
                }
            });

        const rest = new REST().setToken(this.token);
        // console.log(this.slashCommands);
        rest.put(Routes.applicationCommands(this.appId),
            {
                body: this.slashCommandsJSON
            });
    }

    // async initDB(): Promise<void>{
        // this.db =  new (require('sqlite-async'))();
        // await this.db.open(resolve(__dirname, '../sql/data.sqlite'));
        // await tables.forEach(async table => await this.db.run('CREATE TABLE IF NOT EXISTS ' + table ));
    // }

    async runQuery(query: string): Promise<void> {
        // return this.db.run(query);
        await request({method: 'POST', url: `${this.db}/run`,body: query}).catch(e => {
            console.error(e);
            throw new Error(e.message)
        }).then(a=>console.log(a.status));
    }
    async allQuery(query: string): Promise<any[]> {
        const response = await request({
            method: 'POST',
            url: `${this.db}/all`,
            body: query
        }).catch(e => {
            console.error(e);
            throw new Error(e.message)
        });
        if (!response.body) {
            return null;
        }
        let result = null;
        try {
            result = JSON.parse(response.body);
        } catch(e) {
            console.error('PARSING ALL REQUEST, query: ', query, 'response: ',response.body, e);
        }

        return result;
    }
    async getQuery(query: string): Promise<any> {
        const response = await request({
            method: 'POST',
            url: `${this.db}/get`,
            body: query
        }).catch(e => {throw new Error(e.message)});
        if (!response.body) {
            return null;
        }
        let result = null;
        try {
            result = JSON.parse(response.body);
        } catch(e) {
            console.error('PARSING GET REQUEST, query: ', query, 'response: ',response.body, e);
        }

        return result;
    }
    async log(username: string, guild: string, operation: string, error: string) {
        this.runQuery(`INSERT INTO log VALUES ("${username}", "${guild}", "${operation}", "${error}")`);
    }
}

