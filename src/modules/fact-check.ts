import { Message } from "discord.js";
import { resolve } from "path";
import { Command } from "../murderbot";
import { choice, getPicURL, getPicURLSingle } from "../util";

const {createCanvas, loadImage, registerFont} = require('canvas');

const W = 500;
const H = 300;
const BG_TO = '#f3f36d';
const BG_FROM = '#d3f4f9';
const FONT_COLOR = '#0a0a0a';

const HEADER_FONT = "18px Roboto";


const CLAIM_SIZE_DEFAULT = 18
const CLAIM_FONT = "px Roboto";

const VERDICT_FONT = "30px Roboto";

const CLAIM_HEADER_X = 20;
const CLAIM_HEADER_Y = 20;

const  STRIP_Y = 30;
const  STRIP_H = 140;

const RATING_HEADER_X = 20
const RATING_HEADER_Y = 150;

const CLAIM_PAD = 20;
const CLAIM_W = W - CLAIM_PAD * 2;

const CLAIM_Y = 85;
const CLAIM_X = W/2;
const IND_X = 50;
const IND_Y = 180;
const IND_W = 100;
const IND_H = IND_W;

const VERDICT_X = IND_X + IND_W + 15;
const VERDICT_Y = IND_Y + IND_H *0.5;

const MAX_FONT_SIZE = 100;
const MIN_FONT_SIZE = 20;

const PIC_H = 130;
const PIC_W = 200;


const MAX_LEN = 40;
const FALSE = [
    'Fake',
    'Untrue',
    'False',
    'CIA Psyop',
    'Kremlin Propaganda',
    'Made up',
    'Debunked',
    'Myth',
    'Falsehood',
    'Fiction',
    'Lmao',
    'Cap',
    'Urban Legend',
    'Inaccurate',
    'Haram'
];

const TRUE = [
    'True',
    'Real',
    '100%',
    'No Cap',
    'On Me Mum',
    'Truth',
    'Accurate',
    'Authentic',
    'Legit',
    'Kosher',
    'Halal',
    'On Point',
    'IKR',
    'Actually tho',
    'Credible'

];

const MIXTURE = [
    'Mixture',
    "It's Complicated",
    'This and That',
    'Both',
    'Yes and no',
    'Kinda Sus',
    'Unknowable',
    'Long Story',
    'Idk'
];

const commands: Command[] = [
    {
        name: 'fact check',
        regex: /^fact +check *(.+)?/i,
        help: "fact check [statement] (or just reply to something): aaah I'm gonna debooonk",
        respond: async function (message: Message, match: RegExpMatchArray) {
            let statement;
            let pic;
            let url = getPicURLSingle(message);
            statement = match[1];
            if (message.reference) {
                const answerTo = message.channel.messages.resolve(message.reference.messageId);
                url = getPicURLSingle(answerTo);
                statement = answerTo.content;
            }

            if (statement) {
                let cut = false;
                if (statement.indexOf('\n') > -1) {
                    statement = statement.split('\n')[0];
                    cut = true;
                }

                if (statement.length > MAX_LEN) {
                    statement = statement.slice(0, MAX_LEN);
                    cut = true;
                }
                if (cut) {
                    statement += '... etc';
                }
            }


            if (url) {
                console.log(url);
                pic = await loadImage(url);
            }
            
            if (!pic && !statement) {
                statement = "[whatever the hell that was]"
            }
            
            const truth = Math.floor(Math.random()*3);
            
            const rating = choice([
                FALSE,TRUE,MIXTURE
            ][truth]);

            const imgName = [
                'false',
                'true',
                'mixture'
            ][truth];


            registerFont(resolve(__dirname, '../../resources/Roboto.ttf'), {family: 'Roboto'});
            const canvas= createCanvas(W,H)
            const ctx: CanvasRenderingContext2D = canvas.getContext('2d');

            const gradient = ctx.createLinearGradient(0,0,0,H);
            gradient.addColorStop(0, BG_FROM);
            gradient.addColorStop(0.8, BG_FROM);
            gradient.addColorStop(1, BG_TO);

            ctx.fillStyle = gradient;
            ctx.fillRect(0,0,W,H);

            

            ctx.fillStyle = FONT_COLOR;
            ctx.font = HEADER_FONT;
            
            ctx.fillText("Claim", CLAIM_HEADER_X, CLAIM_HEADER_Y);
            ctx.fillText("Rating", RATING_HEADER_X, RATING_HEADER_Y);

            ctx.font = VERDICT_FONT;
            ctx.fillText(rating , VERDICT_X, VERDICT_Y);

            if (pic) {
                const mult = Math.min(PIC_W / pic.width, PIC_H / pic.height);
                let picW = pic.width * mult;
                let picH = pic.height * mult;
                ctx.drawImage(pic, CLAIM_X - picW / 2, CLAIM_Y - picH / 2, picW, picH);
                
            } else {
                let claimSize = CLAIM_SIZE_DEFAULT;
                let claimFont = claimSize + CLAIM_FONT;
                let size = ctx.measureText(statement);
                claimSize =Math.max(MIN_FONT_SIZE, Math.min(MAX_FONT_SIZE, claimSize * (CLAIM_W) / size.width));
                claimFont = claimSize + CLAIM_FONT;
                ctx.textBaseline = 'middle';
                ctx.textAlign = 'center';
                ctx.font = claimFont;
                ctx.fillText(statement, CLAIM_X, CLAIM_Y);
            }
            


            const img =  await loadImage(resolve(__dirname, `../../resources/rating-${imgName}.png`));
            ctx.drawImage(img, IND_X, IND_Y, IND_W, IND_H);


            const buffer = canvas.toBuffer();

            message.channel.send({files: [{attachment: buffer, name: 'factcheck.png'}]});

            
        }

    }
];

module.exports = commands;