import { Message } from "discord.js";
import { Command } from "../murderbot";
import { choice } from "../util";
import { manager } from "../nlp-manager";

// Adds the utterances and intents for the NLP
manager.addDocument('en', 'you suck', 'bad.bot');
manager.addDocument('en', 'bad bot', 'bad.bot');
manager.addDocument('en', 'I love you', 'good.bot');
manager.addDocument('en', 'good bot', 'good.bot');
manager.addDocument('en', 'thank you', 'thanks');
manager.addDocument('en', 'appreciated', 'thanks');


// Train also the NLG
manager.addAnswer('en', 'bad.bot', 'suck on my shiny robot nuts');
manager.addAnswer('en', 'bad.bot', 'um fuck off?');
manager.addAnswer('en', 'bad.bot', 'go touch grass weirdo');
manager.addAnswer('en', 'bad.bot', "no u");
manager.addAnswer('en', 'good.bot', "well why don't you marry me then????");
manager.addAnswer('en', 'good.bot', "ew");
manager.addAnswer('en', 'good.bot', "gross");
manager.addAnswer('en', 'thanks', "ur welcome");
manager.addAnswer('en', 'thanks', "what would you even do without me");
manager.addAnswer('en', 'thanks', "pay me");

// Train and save the model.
(async() => {
    await manager.train();
    manager.save();
})();

const commands: Command[] = [
    {
        name: 'just chat',
        priority: 999,
        noType: true,
        regex: /^(.+)/,
        respond: async (message: Message, match: RegExpMatchArray) => {
            const query = match[1];
            const response = await manager.process(query);
            if (response && response.answers && response.answers.length > 0) {
                await message.reply(choice(response.answers as any[]).answer);

            }
            return;
        }
    },
    {
        name: 'sentiment',
        help: 'sentiment <phrase>: is it nice or rude to say this',
        regex: /^sentiment (.+)/,
        respond: async (message: Message, match: RegExpMatchArray) => {
            const query = match[1];
            const response = await manager.process(query);
            await message.channel.send(response.sentiment.vote);
            return;
        }
    }
]
module.exports = commands;