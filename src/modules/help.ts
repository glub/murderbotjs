import { Message } from 'discord.js';
import { Command } from './../murderbot';

const commands: Command[] = [
    {
        name: 'help',
        regex: /^help( +(.+))?$/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const query = match[2];
            const pages = this.getHelpPages(query);
            for (let i = 0; i < pages.length; i++) {
                await message.channel.send(pages[i]);
            }
        }
    }
];

module.exports = commands;