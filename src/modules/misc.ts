import { Command, CPU_USAGE_ENTRIES, CPU_USAGE_INTERVAL } from './../murderbot';
import { APIEmbed, Message } from 'discord.js';
import { resolve } from 'path';
import { promisify } from 'util';
import { stat } from 'fs';

const statAsync = promisify(stat);



import * as os from 'os';
import { choice } from '../util';
const {registerFont, createCanvas} = require('canvas');
const request = promisify(require('request'));

const CPU_CHART_W: number = 300;
const CPU_CHART_H: number = 100;
const CPU_CHART_PADDING: number = 10;

const CPU_CHART_FULL_W: number = CPU_CHART_W + CPU_CHART_PADDING * 2;
const CPU_CHART_FULL_H: number = CPU_CHART_H + CPU_CHART_PADDING * 2;

const CPU_CHART_INTERVAL_Y: number = CPU_CHART_H / 100;
const CPU_CHART_INTERVAL_X: number = 0 | (CPU_CHART_W / (CPU_USAGE_ENTRIES - 1));


const keys = require('../../keys.js');

const commands: Command[] = [
    {
        name: 'yiff',
        help: 'yiff [@mention someone or write whatever], performs a yiff, use at your own risk',
        regex: /^yiff *(.+)?/,
        respond: async function(message: Message, match: RegExpMatchArray) {
            let target = match[1];
            let caller = message.author.username;

            // if (message.guild) {
            //     if (message.mentions?.users) {
            //         target = (await message.guild.members.fetch(message.mentions.users.first())).nickname;
            //     }
            //     caller = (await message.guild.members.fetch(message.author)).nickname;
            // } else {
            //     caller = message.author.username;;
            // }

            target = target ?? caller;
            
            if (['you', 'murderbot','mb','yourself', 'u', 'urself'].indexOf(target) > -1) {
                target = 'me';
            }

            const yiff = {
                ...this.datasets.yiff,
                caller: [caller],
                target: [target]
            };



            


            const replace = str => {
                return (str as string).replace(/{([a-zA-Z0-9_]+)}/g, (match, item) => {
                    console.log(str, item);
                    return replace(choice(yiff[item]));
                });
            }
            let line: string = replace(choice(yiff.yiff));
            line = line.replace(/{([(a-zA-Z0-9|]+)}/g, (match, item) => {
                console.log(line, item, 'wooo')
                return choice(item.split('|'));
            });

            message.channel.send(line);
        }

    },
    { 
        name: 'invite',
        regex: /^invite(\s+link)?/i,
        help: 'invite link: link for adding murderbot to guilds',
        respond: async function(message: Message)  {
            await message.channel.send(
                `https://discordapp.com/api/oauth2/authorize?client_id=${keys.clientID}&scope=bot&permissions=8`);
        }
    },{
        name: 'status',
        help: 'status: shit no one cares about',
        regex: /^status/i,
        respond: async function(message: Message)  {
            const {rss, heapTotal, heapUsed } = process.memoryUsage();
            const canvas = createCanvas(CPU_CHART_FULL_W, CPU_CHART_FULL_H);
            const ctx = canvas.getContext('2d');
            ctx.fillStyle = '#000c1e';
            ctx.fillRect(0,0,CPU_CHART_FULL_W, CPU_CHART_FULL_H);
            drawGrid(ctx);
            ctx.fillStyle = 'white';
            ctx.beginPath();
            ctx.fillText(
                `CPU Usage over the last ${((CPU_USAGE_ENTRIES - 1) * CPU_USAGE_INTERVAL / 3600000).toFixed(2)} hours`,
                 CPU_CHART_PADDING, CPU_CHART_PADDING * 2);
            ctx.font = '15px Courier';
            ['system', 'user']
                .forEach((kind: 'user' | 'system') => {
                    const color = kind === 'user' ? 'white' : 'red';
                    ctx.strokeStyle = color;
                    ctx.beginPath();
                    ctx.moveTo(CPU_CHART_PADDING, CPU_CHART_PADDING + CPU_CHART_H - this.cpuUsage[0][kind] * CPU_CHART_INTERVAL_Y);
                    this.cpuUsage
                        .slice(1)
                        .forEach((entry, j) => {
                            ctx.lineTo(
                                CPU_CHART_PADDING + (j + 1) * CPU_CHART_INTERVAL_X, 
                                CPU_CHART_PADDING + CPU_CHART_H - entry[kind] * CPU_CHART_INTERVAL_Y
                                );
                        });
                    ctx.stroke();
            });
            const buffer = canvas.toBuffer();
            const db = await statAsync(resolve(__dirname, '../../sql/data.sqlite'));

            const embeds: APIEmbed[] = [{
                color:0xFF0000,
                fields: [
                    {
                        name: 'Uptime',
                        value: (process.uptime()/3600).toFixed(2) + 'h'
                    },
                    {
                        name: 'Memory Usage',
                        value: (rss >> 20) + 'MB'
                    },
                    {
                        name: 'Platform',
                        value: os.platform()
                    },
                    {
                        name: 'Type',
                        value: os.type
                    },
                    {
                        name: 'Guilds',
                        value: this.client.guilds.array().length
                    },
                    {
                        name: 'Ping',
                        value: (0|this.client.ping).toString()
                    },
                    {
                        name: 'Commands',
                        value: this.commands.length
                    },
                    {
                        name: 'Database',
                        value: (db.size >> 20) + 'MB'
                    },
                    {
                        name: 'Mercy',
                        value: 'Never'
                    },
                ]
            }];
            await message.channel.send({embeds})
                .catch(e => console.log(e));
            await message.channel.send({files: [{attachment: buffer, name: 'cpu.png'}]});
        }
    }
];

function drawGrid(ctx: any) {
    for(let i=0;i<=1; i+=0.1) {
        ctx.strokeStyle = '#083454';
        ctx.beginPath();
        ctx.moveTo(CPU_CHART_PADDING, i * CPU_CHART_H + CPU_CHART_PADDING);
        ctx.lineTo(CPU_CHART_PADDING + CPU_CHART_W, i * CPU_CHART_H + CPU_CHART_PADDING);
        ctx.stroke();
    }
    for(let i=0.05;i<1; i+=0.1) {
        ctx.strokeStyle = '#062d49';
        ctx.beginPath();
        ctx.moveTo(CPU_CHART_PADDING, i * CPU_CHART_H + CPU_CHART_PADDING);
        ctx.lineTo(CPU_CHART_PADDING + CPU_CHART_W, i * CPU_CHART_H + CPU_CHART_PADDING);
        ctx.stroke();
    }
}

module.exports = commands;