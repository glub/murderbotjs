import { Command } from "../murderbot";
import { Message } from "discord.js";
import { resolve } from "path";
import { choice } from "../util";
const { createCanvas, loadImage, Image, registerFont } = require("canvas");
const commands:Command[] = [

    {
        name: 'add comic template',
        help: 'adds a template for mb comic command, go here for instructions https://maxbalandin.su/comic-template/',
        regex: /^add comic template (\S+) (\S+) (\S+)/i,
        respond: async function(message: Message, match: RegExpMatchArray) {
            let name = match[1];
            let url = match[2];
            if (url[0] === '<') {
                url = url.slice(1,-1);
            }
            let bubbles = match[3];
            

            if (name === 'NAME_HERE') {
                message.channel.send('You forgot to change the name you ABSOLUTE DONG');
                return;
            } else if (name.toLowerCase() === 'templates') {
                message.channel.send(`"templates" is reserved for the template list command BITCH`);
                return;
            } else if (name.match(/^\d+$/)) {
                message.channel.send("the name can't be just numbers. bitch");
                return;

            } else {
                const names = await this.allQuery('SELECT name from comic_templates');
                if (names.indexOf(name) > -1) {
                    message.channel.send("name already exists. *sad trombone*");
                    return;
                }
            }
            try {
                
                const image = await loadImage(url);
                const {width, height} = image;
                const canvas = createCanvas(width, height);
                const ctx = canvas.getContext('2d');
                ctx.drawImage(image, 0,0);
                const img = canvas.toDataURL('image/jpeg');
                let query = `INSERT INTO comic_templates VALUES("${img}", "${name}", "${bubbles}")`;
                console.log(query); 
                await this.runQuery(query);
                message.channel.send('Added');
            } catch(e) {
                message.channel.send('Error ' + e);
            }
        }
    }, {
        name: 'delete comic template',
        admin: true,
        regex: /delete +template +(\S+)/,
        respond: async function(message: Message, match: RegExpMatchArray) {
            const name = match[1];
            const template = await this.runQuery(`delete from comic_templates where name = '${name}'`);

            console.log(template);
            if (!template) {
                message.reply('aight done');
            } else {
                message.reply('nah something aint right');
            }
        }

    },


    {
        name: 'comic',
        help: 'comic: generate a comic based on the last couple messages. "comic [template]" to use a specific template, "comic [number] to use one with a specific number of speech bubbles, "comic templates" to list available ones.',
        regex: /^comic( +(\S+))?$/i,
        respond: async function(message: Message, match: RegExpMatchArray) {
            const picked = match[2];
            let num = parseInt(picked);
            let template;
            if (picked === 'templates') {
                let templates = await this.allQuery(`SELECT name FROM comic_templates`);
                message.channel.send(templates.map(t => t.name).join(', '));
                return;
            } else if (picked && picked.match(/^\d+$/)) {
                const all = await this.allQuery(`SELECT * FROM comic_templates`);
                
                
                const fitting = all.filter(t => JSON.parse(t.bubbles).length === num);
                if (fitting.length === 0) {
                    message.channel.send(`There are no templates with exactly ${num} speech bubbles you get a random one instead`);
                    template = choice(all);
                } else {
                    template = choice(fitting);
                }
            } else if (picked) {
                template = await this.getQuery(`SELECT * FROM comic_templates where name = "${picked}" LIMIT 1`);
                if (!template) {
                    message.channel.send(`no such template, here's a random instead, fuck you`);
                }
            }
            if (!template) {
                template = await this.getQuery('SELECT * FROM comic_templates ORDER BY RANDOM() LIMIT 1');
            }
            
            const {image: img, name, bubbles: rectsJson} = template;
            const rects = JSON.parse(rectsJson);
            const image = new Image();
            await new Promise<void>(resolve => {
                image.onload =  () => {
                    resolve();
                }
                image.src = img;
            });
            const {width, height} = image;
            await registerFont(resolve(__dirname, '../../resources/Cousine-Bold.ttf'), {family: 'Cousine'});
            const canvas = createCanvas(width, height);
            const ctx = canvas.getContext('2d');
            ctx.drawImage(image, 0,0);
            const start = message.reference?.messageId;

            let limit = rects.length;
            if (start) {
                limit--;
            }
            const messages = Array.from(
                (await message.channel.messages.fetch({
                    limit,
                    after: start,
                    before: message.id
                
                })).values() as unknown as  ArrayLike<Message>);



            if (start) {
                let ref = await message.channel.messages.fetch(start);
                messages.push(ref);
            }
           
            // console.log(last, index);
            messages.reverse();

            for(let i = 0; i<messages.length;i++) {
                // const line = await message.channel.messages.fetch(messages[i].id);
                const line = messages[i];
                let text = line.content;
                const attachment = line.attachments 
                                && line.attachments.first()
                                && line.attachments.first().url;
                const embed = line.embeds 
                                && line.embeds[0]
                                && line.embeds[0].thumbnail
                                && line.embeds[0].thumbnail.url;
                const picture = attachment || embed;
                const mentions =　line.mentions;
                if (mentions) {
                    const users = Array.from(mentions.users.values());
                    const roles = Array.from(mentions.roles.values());
                    for (let j = 0; j < users.length; j ++) {
                        const user = users[j];
                        let name = user.username;
                        if (message.guild) {
                            name = (await message.guild.members.fetch(user.id)).displayName;
                        }
                        // console.log(name, line.content, new RegExp("<@"+user.id+">", "g"));
                        text = text.replace(new RegExp("<@"+user.id+">", "g"), name);
                    }
                    for (let j = 0; j < roles.length; j ++) {
                        const role = roles[j];
                        let name = role.name;
                        text = text.replace(new RegExp("<@"+role.id+">", "g"), name);
                    }
                }
                const [px,py,pw,ph,side,position] = rects[i];
                let x = px / 100 * width;
                let y = py / 100 * height;
                let w = pw / 100 * width;
                let h = ph / 100 * height;
                const ratio = w/h;
                ctx.fillStyle = 'white';
                ctx.strokeStyle = 'black';
                ctx.lineWidth = 3;
                ctx.fillRect(x,y,w,h);
                ctx.strokeRect(x,y,w,h);

                const dx = width * 0.02;
                const dy = height * 0.02;
                ctx.beginPath();
                if (side === 0) {
                    const y0 = y + 2;
                    const x0 = x + w * position / 100;
                    ctx.moveTo(x0 + dx, y0);
                    ctx.lineTo(x0, y0 - dy);
                    ctx.lineTo(x0 - dx, y0);
                } else if (side === 1) {
                    const y0 = y + h * position / 100;
                    const x0 = x + w - 2;
                    ctx.moveTo(x0, y0 - dy);
                    ctx.lineTo(x0 + dx, y0);
                    ctx.lineTo(x0, y0 + dy);
                } else if (side === 2) {
                    const y0 = y + h - 2;
                    const x0 = x + w * position / 100;
                    ctx.moveTo(x0 + dx, y0);
                    ctx.lineTo(x0, y0 + dy);
                    ctx.lineTo(x0 - dx, y0);
                } else if (side === 3) {
                    const y0 = y + h * position / 100;
                    const x0 = x + 2;
                    ctx.moveTo(x0, y0 - dy);
                    ctx.lineTo(x0 - dx, y0);
                    ctx.lineTo(x0, y0 + dy);
                }
                ctx.lineWidth = 3;
                ctx.stroke();
                ctx.closePath();
                ctx.fill();
                const padding = 0.01 * Math.min(width, height);
                w -= padding * 2;
                h -= padding * 2;
                y += padding;
                x += padding;
                ctx.fillStyle = 'black';


                if (picture) {
                    const img = await loadImage(picture);
                    const {width: tw, height: th} = img;
                    const hratio = h/th;
                    const wratio = w/tw;
                    const tratio = Math.min(hratio, wratio);
                    const imgw = tw * tratio;
                    const imgh = th * tratio;

                    const offsetx = (w - imgw) / 2;
                    const offsety = (h - imgh) / 2;
                    ctx.drawImage(img, x + offsetx, y + offsety, imgw, imgh);
                    // ctx.drawImage(img, 0, 0);

                } else {

                    const lineHeight = 1.3;
                    const words = text.split(/\s+/g);
    
                    const bias = 3;
                    const length = text.length * ratio * lineHeight * bias;
    
                    const maxWidth = Math.max(Math.floor(Math.sqrt(length)), ...words.map(w => w.length));
                    ctx.textBaseline = 'top';
    
                    const bubble = [];
                    let bubbleLine = '';
                    words.forEach((word, i) => {
                        let potential = bubbleLine ? bubbleLine + ' ' + word : word;
                        if (potential.length > maxWidth) {
                            bubble.push(bubbleLine);
                            bubbleLine = word;
                        } else {
                            bubbleLine = potential;
                        }
                        if (i === words.length - 1) {
                            bubble.push(bubbleLine);
                        }
                    });
    
                    const vertical = h / bubble.length / lineHeight;
                    const fontCorrection = 1.6;
                    const actualMaxWidth = Math.max(...bubble.map(b => b.length));
                    const horizontal = w / actualMaxWidth * fontCorrection;
                    
                    const font = Math.floor(Math.min(vertical, horizontal));
    
                    ctx.font = `${font}px Cousine`;
    
                    const hspace = bubble.length * font * lineHeight;
                    const wspace = actualMaxWidth * font / fontCorrection;
                    const offsety = (h - hspace)/2;
                    const offsetx = (w - wspace)/2;
    
    
                    bubble.forEach((l,i) => {
                        ctx.beginPath();
                        ctx.fillText(l, offsetx + x, offsety + y + i * font * lineHeight);
                    })
                }

                // console.log(line.length, ratio, length, maxWidth, bubble, font, vertical, horizontal)

            };
            const buffer = canvas.toBuffer();
            message.channel.send({files: [{attachment: buffer, name: 'comic.png'}]});
        }
    }
];

module.exports = commands;