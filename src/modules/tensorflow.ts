
import { Message } from "discord.js";
import { Command } from "../murderbot";
import { getPicURL } from "../util";
const  { createCanvas, loadImage, Image, registerFont } = require('canvas');

const coco = require('@tensorflow-models/coco-ssd');
const tf = require('@tensorflow/tfjs-node');
// (async () => {
//     console.log("loading model...");
    
//     console.log("model loaded...");

// })();

const commands: Command[] = [
     {
        name: "tflow",
        regex: /^tflow/,
        respond: async function(message: Message) {
            // if (model) {
            // } else {
                await message.channel.send('model is loading')
                let model = await coco.load();
                await message.channel.send('model loaded')
                const url = await getPicURL(message);
                const img = await loadImage(url);
                const canvas = createCanvas(img.width, img.height);
                const ctx = canvas.getContext('2d');
                ctx.drawImage(img,0, 0);
                const data = ctx.getImageData(0,0,img.width, img.height).data;
                const predictions = await model.detect(tf.node.decodeImage(data));
                console.log(predictions);
            // }

        }

     }
];

module.exports = commands;