import { Command } from "../murderbot";
import { APIEmbed, Message } from "discord.js";
import { IamAuthenticator } from 'ibm-watson/auth';
const NaturalLanguageUnderstandingV1 = require('ibm-watson/natural-language-understanding/v1');
const LanguageTranslatorV3 = require('ibm-watson/language-translator/v3');
// const ToneAnalyzerV3 = require('ibm-watson/tone-analyzer/v3');
const keys = require('../../keys.js').ibm;

const descibe = (tone_id, score) => {
    return ((score > 0.8 && 'very')
            || (score > 0.5 && 'pretty')
            || (score > 0.2 && 'kinda')
            || 'a bit')
            + ' ' +
        ({
        anger: 'angry',
        disgust: 'disgusted',
        fear: 'scared',
        joy: 'happy',
        sadness: 'sad'
    }[tone_id] || tone_id);
}


const url = {
    tone: 'https://gateway-fra.watsonplatform.net/tone-analyzer/api',
    translate: 'https://gateway-fra.watsonplatform.net/language-translator/api',
    analyze: 'https://gateway-fra.watsonplatform.net/natural-language-understanding/api'
}

const version = {
    tone: '2017-09-21',
    translate: '2018-05-01',
    analyze: '2019-07-12'
}


const model = {
    // tone: ToneAnalyzerV3,
    analyze: NaturalLanguageUnderstandingV1,
    translate: LanguageTranslatorV3
}

type IBMAPI = 'analyze' | 'translate'; // | 'tone'

const api = (name: IBMAPI) => new (model[name])({
    version: version[name],
    authenticator: new IamAuthenticator({
        apikey: keys[name]
    }),
    url: url[name]
});

const naturalLanguageUnderstanding = api('analyze');
// const toneAnalyzer = api('tone');
const languageTranslator = api('translate');

interface LanguageCache {
    list: {code: string, name: string}[];
    models: {target: string, source: string}[];
    codes: {[x: string]: string};
}
let languageCache: LanguageCache = {
    list: null,
    models: null,
    codes: null
};



async function getLanguages(): Promise<LanguageCache> {
    if (!languageCache.codes) {
        const response = await languageTranslator.listIdentifiableLanguages().catch(err => { throw new Error(err.message)});
        const modelsResponse = await languageTranslator.listModels({target: 'en'}).catch(err => {throw new Error(err.message)});
        const models = modelsResponse.result.models.map(m => ({source: m.source, target: m.target}));
        const list = response.result.languages.map(el => ({name: el.name.toLowerCase().replace(' ', '_'), code: el.language}))
        const codes = list.reduce((acc, cur) => ({...acc, [cur.name]: cur.code}), {});
        languageCache = {models, codes, list};
    }
    return languageCache;
}


const commands: Command[] = [
    {
        name: 'languages',
        help: 'languages: list of languages translation api supports',
        regex: /^languages/i,
        respond: async function(message: Message) {
            const cache = await getLanguages();
            await message.channel.send(cache.list.map(lang => lang.name).join(', '));
        }
    },
    {
        name: 'translate',
        help: 'translate [from: language] [to: language] <something>: just that',
        regex: /^translate( +from +(\S+?))?( +to +(\S+))? +(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray) {
            const text = match[5];
            const cache = await getLanguages();
            let source = cache.codes[match[2]];
            const target = cache.codes[match[4]] || 'en';
            if (!source) {
                const resp = await languageTranslator.identify({text});



                if (resp.result.languages.length === 0) {
                    await message.channel.send("I don't speak whatever the hell that is");
                }
                resp.result.languages.sort((a,b) => b.confidence - a.confidence);

                source = resp.result.languages[0].language;
            }

            const src = cache.list.find(lang => lang.code === source).name;
            const tgt = cache.list.find(lang => lang.code === target).name;
            if (!cache.models.find(model => model.source === source && model.target === target)) {
                await message.channel.send(`I can't do ${src}-${tgt} combination sorry broo`);
                return;
            }

            const response = await languageTranslator.translate({
                text,
                source,
                target
            }).catch(err => { throw new Error(err.message)});
            if (!response.result.translations.length) {
                await message.channel.send("sorry chief can't translate that");
                return;
            }

            const translation = response.result.translations[0].translation;
            const embeds: APIEmbed[] = [{
                title: translation,
                fields: [
                    {
                        name: 'from', 
                        value: src

                    },{
                        name: 'to', 
                        value: tgt
                    }
                ]
            }];
            await message.channel.send({embeds});
            
            // console.log(response, response.result.translations)
        }
    },
    // {
    //     name: 'tone',
    //     help: 'tone <something>: sentiment analysis',
    //     regex: /^tone +(.+)/,
    //     respond: async function(message: Message, match: RegExpMatchArray) {
    //         const toneInput = match[1];
    //         const response = await toneAnalyzer.tone({
    //             toneInput,
    //             tones: ['emotional', 'language'],
    //             contentType: 'text/plain'
    //         }).catch(err => { throw new Error(err.message); } );
    //         await message.channel.send(response.result.document_tone.tones.map(el => descibe(el.tone_id, el.score)).join(', ') || 'meh');
    //     }
    // },
    {
        name: 'analyze',
        help: 'analyze <something>: analyze walls of text',
        regex: /^analyze +(.+)/,
        respond: async function(message: Message, match: RegExpMatchArray) {
            const text = match[1];
            const response = await naturalLanguageUnderstanding.analyze({
                text,
                features: {
                  entities: {
                    emotion: true,
                    limit: 5,
                  },
                  concepts: {
                      limit: 3
                  },
                  categories: {
                      limit: 1
                  }
                },
            });
            console.log(JSON.stringify(response, null, 2));
            const {entities, concepts, categories} = response.result;

            const camelSpotter = /([a-z])(?=[A-Z])/g;
            const camelKiller = str => str.replace(camelSpotter, '$1 ').toLowerCase();

            let essay = '';
            if (categories && categories.length > 0) {
                essay+=`This text is about ${categories[0].label}\n`;
            }

            if(concepts && concepts.length > 0) {
                essay+=`The text touches upon the concepts of: ${concepts.map(c => camelKiller(c.text)).join(', ')}\n`;
            }

            if (entities) {
                entities.forEach(entity => {
                    let disambiguation = '';
                    if (entity.disambiguation && entity.disambiguation.subtype) {
                        disambiguation = ` (${entity.disambiguation.subtype.map(t => camelKiller(t)).join(', ')})`;
                    }
                    essay = essay + `${camelKiller(entity.type)} by the name of ${entity.text}${disambiguation} was spoken of`;
                    if (entity.emotion) {
                        const emotions = Object.entries(entity.emotion);
                        emotions.sort((a,b) => (b[1] as number) - (a[1] as number));
                        const most = emotions[0];

                        const [emotion, score] = most;
                        const level = (score > 0.8 && 'a lot of')
                                        || (score > 0.5 && 'certain')
                                        || (score > 0.3 && 'some')
                                        || 'a bit of';
                        essay = essay + `, with ${level} ${emotion}`;
                    }
                    essay = essay + `.\n`;

                });
            }
            essay = essay || 'not a whole lot to say about that';
            await message.channel.send(essay);

        }
    }
]
module.exports = commands;