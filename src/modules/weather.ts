import { promisify } from 'util';
import { Message } from 'discord.js';
import { resolve } from 'path';
import {createCanvas, registerFont} from 'canvas';
import { Command, Murderbot } from './../murderbot';
const request = promisify(require('request'));

const keys = require('../../keys.js');
const W = 1280;
const H = 512;
const BG = 'black';

const FORECAST = 4;

const DAY = {
    day: 'white',
    morn: 'yellow',
    eve: 'orange',
    night: 'blue'
};

type WeatherProp = 'humidity' | 'pressure' | 'clouds' | 'speed';

const MAX = {
    humidity: 100,
    pressure: 2000,
    clouds: 100,
    speed: 60
};
const COLOR = {
    humidity: '#95e091',
    pressure: '#e8de8b',
    clouds: '#e2a1c9',
    speed: '#9ebfe2'
}


const MAX_TEMP = 50;
const PADDING = 100;


const FULL_W = W + 2* PADDING;
const FULL_H = H + 2* PADDING;

interface Day {
    clouds: number;
    speed: number;
    dt:number;
    temp: {
        day: number,
        night: number,
        eve: number,
        morn: number
    };
    pressure: number;
    humidity: number;
    weather: {
        main: string,
        description: string
    }[]
}
type TimeOfDay = 'morn' | 'day' | 'eve' | 'night';




const commands: Command[] = [
    {
        name: 'location',
        help: 'weather location <city>: saves your location to use for weather command',
        regex: /^weather\s+location\s+(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const city = match[1];
            try{
                await this.runQuery(`INSERT INTO locations VALUES ("${message.author.id}", "${city}")`);
            }catch(e){
                try{
                    await this.runQuery(`UPDATE locations SET city="${city}" WHERE userID="${message.author.id}"`);
                }catch(f){
                    throw new Error('Error saving location.');
                }
                
            }
            await message.channel.send(`you live in ${city} now. man, what a heckhole`);
        }
    }, {
        name: 'weather',
        help: 'weather [<number> days] [<city>]: weather forecast',
        regex: /^weather(\s+(\d+)\s+days)?(\s+(.+?))?$/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            let city = match[4];
            if(!city) {
                const db = await this.getQuery(`SELECT city FROM locations WHERE userID="${message.author.id}"`);
                if(!db){
                    await message.channel.send('"mb weather" where? name a city or save a location you pizza shit');
                    return;
                }
                city = db.city;
            }
            const days = match[2] || FORECAST;
            const url = `http://api.openweathermap.org/data/2.5/forecast/daily?q=${encodeURI(city)}&APPID=${keys.weather}&units=metric&cnt=${days}`;
            const response = await request(url);
            const weather = JSON.parse(response.body);
            registerFont(resolve(__dirname, '../../resources/Roboto.ttf'), {family: 'Roboto'});
            const canvas = createCanvas(FULL_W,FULL_H);
            const ctx = canvas.getContext('2d');

            ctx.fillStyle = BG;
            ctx.fillRect(0,0,FULL_W,FULL_H);
            plot(ctx, weather.list);
            ctx.fillStyle = 'white';
            ctx.textAlign = 'left';
            ctx.font = '18px "Roboto"'
            const caption = `${weather.city.name}, ${weather.city.country} (${coords(weather.city.coord.lat)}N, ${coords(weather.city.coord.lon)}E)`;
            ctx.fillText(caption, (PADDING*0.3)|0, (PADDING*0.3)|0)
            const buffer = canvas.toBuffer();
            await message.channel.send(
                {files: [{attachment: buffer, name: 'forecast.png'}]});
        }
    }

];
module.exports = commands;
function plotDays(ctx: any, list: Day[]) {
    ctx.strokeStyle = DAY;
    const INTERVAL = W / (list.length - 2);
    list.forEach((day, i) => {
        ['morn', 'day', 'eve', 'night']
            .forEach((time: TimeOfDay, j) => {
                ctx.strokeStyle = DAY[time];
                let x = (i + j / 4) * INTERVAL;
                ctx.beginPath();
                ctx.moveTo(x, 0);
                ctx.lineTo(x, H);
                ctx.stroke();
            });
    });
}

function plot(ctx: any, list: Day[]): void {
    let grd = ctx.createLinearGradient(0, PADDING, 0, H);
    grd.addColorStop(0, "#ff1c1c");
    grd.addColorStop(0.2, "#ff981c");
    grd.addColorStop(0.4, "#f2e774");
    grd.addColorStop(0.6, "#c2eae8");
    grd.addColorStop(1, "#adb4ff");
    ctx.fillStyle = grd;
    ctx.fillRect(0,0, FULL_W, FULL_H);
    ctx.fillStyle = 'rgba(0,0,0,0.9)';
    ctx.fillRect(0,0,FULL_W, FULL_H);
    
    plotLine(ctx, list, 'humidity');
    plotLine(ctx, list, 'speed');
    plotLine(ctx, list, 'pressure');
    plotLine(ctx, list, 'clouds');
    ctx.beginPath();
    ctx.strokeStyle = grd;
    ctx.fillStyle = grd;
    const WEEK = [
        'Mon',
        'Tue',
        'Wed',
        'Thu',
        'Fri',
        'Sat',
        'Sun'
    ];
    ctx.lineWidth = 2;
    const mult = H / MAX_TEMP / 2;
    const INTERVAL = W / (list.length - 0.25);
    const OFFSET = H*0.5;
    let labels: {t: number, x: number, y: number}[] = [];
    list.forEach((day, i) => {
            ['morn', 'day', 'eve', 'night']
                .forEach((time: TimeOfDay, j) => {
                    let x = (PADDING + (i + j / 4) * INTERVAL)|0;
                    let y = (PADDING + OFFSET - day.temp[time] * mult)|0;
                    labels.push({x,y,t: 0|day.temp[time]});
                    i || j? ctx.lineTo(x,y) : ctx.moveTo(x,y);
                });
    });
    ctx.stroke();

    ctx.textAlign = 'center';
    labels.forEach((label, i) => {
        ctx.beginPath();
        ctx.fillStyle = grd;
        ctx.arc(label.x, label.y, 6, 0, Math.PI*2);
        ctx.fill();
        ctx.fillStyle = 'white';
        ctx.beginPath();
        ctx.font = '15px "Roboto"';
        ctx.fillText(`${label.t}°C`, label.x, label.y - 40);
        ctx.beginPath();
        ctx.fillText(`${toF(label.t)}°F`, label.x, label.y - 20);
        if(!(i % 4)) {
            let index = 0|(i/4);
            ctx.font = '25px "Roboto"';
            ctx.beginPath();
            const WDAY =  WEEK[new Date(list[index].dt*1000).getDay()];
            ctx.fillText(`${WDAY}`, label.x+ INTERVAL*0.4, label.y + 40);
            const day = list[index];
            const desc = day.weather[0].description || day.weather[0].main;
            ctx.font = '15px "Roboto"';
            ctx.beginPath();
            ctx.fillText(desc, label.x+ INTERVAL*0.4, label.y + 60);
        }
        
    });
}




function plotLine(ctx: any, list: Day[], prop: WeatherProp): void {
    ctx.strokeStyle = COLOR[prop];
    ctx.fillStyle = COLOR[prop];
    ctx.font = '15px Roboto';
    ctx.lineWidth = 1;
    const days: {x:number, y: number, day: Day}[] = [];
    ctx.beginPath();
    const INTERVAL = W / list.length;
    list.forEach((day, index) => {
        let i = index + 0.5;
        let x = 0|(INTERVAL * i + PADDING);
        let y = 0|(PADDING + (1 - day[prop] / MAX[prop]) * H);
        days.push({x,y, day});
        i ? ctx.lineTo(x,y) : ctx.moveTo(x,y);
    });
    ctx.stroke();
    days.forEach((element, i) => {
        ctx.beginPath();
        ctx.arc(element.x, element.y ,4,0,Math.PI*2);
        ctx.fill();
        ctx.beginPath();
        ctx.fillText((!i?`${prop}: `:``) + `${element.day[prop]}`, element.x, element.y + 20);
        ctx.fill();
    });
}
function toF(c: number): number {
    return 0|(c * 9/5 + 32);
}
function coords(deg: number): string {
    const whole = 0|deg;
    const min = 0 |((deg - whole) * 60);
    return `${whole}'${min}"`;
}