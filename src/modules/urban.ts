import { promisify } from 'util';
import { Message } from 'discord.js';
import { Command, Murderbot } from './../murderbot';
const request =  promisify(require('request'));
const keys = require('../../keys.js');
const commands: Command[] = [
    {
        name: 'urban dictionary',
        help: 'ud [random|<number>] <whatever>: UD definitions',
        regex: /^ud\s+((random|\d+)\s+)?(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const term = encodeURI(match[3]);
            const response = await request({
                url: `https://mashape-community-urban-dictionary.p.mashape.com/define?term=${term}`,
                headers: {
                    'X-Mashape-Key': keys.mashape,
                    'Accept' : 'text/plain'
                }
            });
            const defs = JSON.parse(response.body).list;
            let index = 0;
            if(match[2]) {
                index = parseInt(match[2]) || ~~(Math.random()*defs.length);
            }
            await message.channel.send(defs[index].definition);
        }
    }
];
module.exports = commands;