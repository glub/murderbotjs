import { Command } from "../murderbot";


const MODELS = {
    glub: ['max'],
    Werd: ['anthony'],
    chez: ['chaz'],
    Eliza: []
}



const commands: Command[] = [
    {
        name: 'finish',
        regex: /^how +would +(\S+) +finish +(.+)/
    }
];

module.exports = commands;