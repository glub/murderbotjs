import { CanvasRenderingContext2D } from "canvas";
import { Message, User } from "discord.js";
import { resolve } from "path";
import { Command } from "../murderbot";
import { rnd } from "../util";
const { createCanvas, loadImage, registerFont} = require("canvas");

const SPECIAL: string[] = [
    "STR",
    "PER",
    "END",
    "CHA",
    "INT",
    "AGI",
    "LCK",
];

const VIBES: string[] = [
    "clown",
    "horny",
    "dumb",
    "feral",
    "cursed",
    "bastard",
    "baby",
];

const TITLES: [number, number, string][] = [
    [0,    0,    "calls mao a lib"],
    [0.25, 0,    "owns a GDR flag"],
    [0.5,  0,    "death penalty\n for jaywalking"],
    [0.75, 0,    "owns a skull\n balaklava"],
    [1,    0,    "writes sequels\n to mein kampf"],
    
    [0,    0.25, "styles moustache\n like parenti"],
    [0.25, 0.25, "emails tweets\n to chomsky"],
    [0.5,  0.25, "wants to be\n a prison warden"],
    [0.75, 0.25, "what if\n jesus had a gun"],
    [1,    0.25, "phrenology\n enthusiast"],
    
    [0,    0.5,  "cook your\n landlord"],
    [0.25, 0.5,  "less blood\n for stocks"],
    [0.5,  0.5,  "just wanna\n grill ffs"],
    [0.75, 0.5,  "more blood\n for stocks"],
    [1,    0.5,  "pay for\n oxygen"],

    [0,    0.75, "CIA psyop\n identifier"],
    [0.25, 0.75, "unionized\n polycules"],
    [0.5,  0.75, "torrents\n free content"],
    [0.75, 0.75, "death\n before taxes"],
    [1,    0.75, "employs\n child soldiers"],

    [0,    1,    "family\n abolition"],
    [0.25, 1,    "be crime\n do gay"],
    [0.5,  1,    "NAP is\n too auth"],
    [0.75, 1,    "800k$\n ape pfp"],
    [1,    1,    "age of consent\n abolition"],
];

const VIBES_N = VIBES.length;
const ATTR_MAX = 5;

const W = 750;
const H = 450;
const PAD = 15;
const AV_SIZE = 160;
const VIBES_PAD = 55;
const VIBE_POINT = 3;
const FONT_NAME = "24px Roboto bold";
const FONT_VIBES = "18px Roboto bold";

const POLITICS_FONT = "15px Roboto bold"
const POLITICS_LINE = 17;


const FONT_SIZE_DND = 18;
const OFFSET_DND = FONT_SIZE_DND * 1.2;
const FONT_DND = FONT_SIZE_DND + "px Roboto bold";
const TAU = Math.PI * 2;

const VIBE_LABEL_OFFSET = 0;

const VIBES_LINE = 1;
const VIBES_GRID = 0.5;
const VIBES_ROTATE = Math.PI / 6;
const GRID_NUM = 5;
const AVATAR_PAD = 5;


const TOP = AV_SIZE + 2 * PAD;
const VIBES_D = H - TOP;

const VIBES_R_OUT = VIBES_D / 2;
const VIBES_R_IN = VIBES_D / 2 - VIBES_PAD;
const VIBES_R_MID = (VIBES_R_OUT + VIBES_R_IN) / 2;

const VIBES_CX = VIBES_R_OUT;
const VIBES_CY = H - VIBES_R_OUT;

const SPEC_LEFT = VIBES_D + 2 *PAD;
const SPEC_BAR_LEFT = SPEC_LEFT + PAD * 2;


const SPEC_MARGIN = 5;
const SPEC_PAD = 2;
const SPEC_H = VIBES_D;
const SPECIAL_N = SPECIAL.length;
const SPEC_ROW = (SPEC_H - 3 * PAD) / SPECIAL_N - SPEC_MARGIN;
const FONT_SPEC = (SPEC_ROW * 0.7).toFixed(2) + "px Roboto bold";
const SPEC_RECT = SPEC_ROW - 2 * SPEC_PAD;

const SPEC_OFFSET = -20;
const VIBE_OFFSET = -5;


const BG_SAT = 15;
const BG_LIT = 80;

const TEXT_SAT = 20;
const TEXT_LIT = 15;

const ACC_SHIFT = 30;

const ACC_SAT = 50;
const ACC_LIT = 40;

const TEXT_ACC_SAT = 30;
const TEXT_ACC_LIT = 20;

const GRID_ALPHA = 0.6;
const ATTR_ALPHA = 0.2;
const VIBE_ALPHA = 0.5;

const AV_RECT_X = PAD - AVATAR_PAD;
const AV_RECT_Y = PAD - AVATAR_PAD;
const AV_RECT_W = 2 * AVATAR_PAD + AV_SIZE;
const AV_RECT_H = 2 * AVATAR_PAD + AV_SIZE;
const DND_OFFSET = 500;

const COMPASS_LEFT = SPEC_BAR_LEFT + (SPEC_ROW + SPEC_PAD) * ATTR_MAX + PAD * 2.5;
const COMPASS_TOP = TOP + 0.1 * VIBES_D;

const COMPASS_SIZE = 0.8 * VIBES_D;

const COMPASS_SQUARE = COMPASS_SIZE / 2;

const AUTH_LEFT = "rgba(255,118,118, 0.7)";
const AUTH_RIGHT = "rgba(64,172,255, 0.7)";
const LIB_LEFT = "rgba(155,238,152, 0.7)";
const LIB_RIGHT = "rgba(193,155,237, 0.7)";

const dndQuery = (seed, func) => `select A.name as clss, 
B.name as item1, 
C.name as item2,
D.name as fursona, 
E.name as power, 
F.name as skill1, 
G.name as skill2  , 
H.name as spell
from 
(select name from class limit 1 offset ( ${func(seed)} % (select count(*) from class))) A,
(select name from equipment limit 1 offset ( ${func(seed + 1)} % (select count(*) from equipment))) B,
(select name from equipment limit 1 offset ( ${func(seed + 2)} % (select count(*) from equipment))) C,
(select name from monster limit 1 offset ( ${func(seed + 3)} % (select count(*) from monster))) D,
(select name from power limit 1 offset ( ${func(seed + 4)} % (select count(*) from power))) E,
(select name from skill limit 1 offset ( ${func(seed + 5)} % (select count(*) from skill))) F,
(select name from skill limit 1 offset ( ${func(seed + 6)} % (select count(*) from skill))) G,
(select name from spell limit 1 offset ( ${func(seed + 7)} % (select count(*) from spell))) H
`;

const commands: Command[] = [
    {
        name: 'rolls dice',
        help: 'roll <N>D[M]: roll N D-M dice',
        regex: /^roll +(\d+)? *D *(\d+)*/i,
        respond: async function(message: Message, match: RegExpMatchArray) {
            const N = parseInt(match[1]) || 1;
            const M = parseInt(match[2]);
            let res = "";
            let sum = 0;
            if (N > 1) {
                for (let i =0; i < N; i++) {
                    const roll = Math.ceil(Math.random()*M);
                    res += roll + ","
                    sum += roll;
    
                }
                res =  sum + " (" + res + ")";
            } else {
                res = "" + Math.ceil(Math.random()*M);
            }

            message.channel.send(res);

        }
    },
    {
        name: 'vibe check',
        help: 'vibe check [mention or reply or neither]: careful analysis',
        regex: /^(stats|vibe +check) */i,
        respond: async function(message: Message, match: RegExpMatchArray) {
            const dnd = this.datasets.dnd; 
            const mention = message.mentions?.users?.first();

            let user: User;
            if (message.reference) {
                const answerTo = await message.channel.messages.resolve(message.reference.messageId);
                user = answerTo.author;
            } else{
                user = mention || message.author;
            }

            
            
            let name = user.username;

            const avatar = await loadImage(user.displayAvatarURL({extension: 'jpg'}));
            if (message.guild) {
                name = message.guild.members.resolve(user).nickname;
            }
            registerFont(resolve(__dirname, '../../resources/Roboto.ttf'), {family: 'Roboto'});
            const canvas = createCanvas(W, H);
            const ctx: CanvasRenderingContext2D = canvas.getContext('2d');
            // DETERMINISTIC APPROACH
            // const id = +(user.id.slice(-5)) + offset;
            // RANDOM EACH TIME
            const id = Math.random() * 10000;

            const clamp = (seed, dx = ATTR_MAX) => Math.round(dx*rnd(seed))/dx;
            const {
                clss,
                fursona,
                item1,
                item2,
                power,
                skill1,
                skill2,
                spell} = await dnd.get(dndQuery(id, num => DND_OFFSET * clamp(num, DND_OFFSET)));

            const bgHue = clamp(id, 10) * 360;

            const accHue = bgHue + ACC_SHIFT;



            const bg = `hsl(${bgHue}, ${BG_SAT}%, ${BG_LIT}%)`;
            const acc = `hsl(${accHue}, ${ACC_SAT}%, ${ACC_LIT}%)`;

            const text = `hsl(${bgHue}, ${TEXT_SAT}%, ${TEXT_LIT}%)`;
            const gridColor = `hsla(${bgHue}, ${TEXT_SAT}%, ${TEXT_LIT}%, ${GRID_ALPHA})`;
            const accText = `hsl(${accHue}, ${TEXT_ACC_SAT}%, ${TEXT_ACC_LIT}%)`;
            
            const vibeColor = `hsla(${accHue}, ${ACC_SAT}%, ${ACC_LIT}%, ${VIBE_ALPHA})`;
            const attrColor = `hsla(${accHue}, ${ACC_SAT}%, ${ACC_LIT}%, ${1})`;
            const noAttrColor = `hsla(${accHue}, ${ACC_SAT}%, ${ACC_LIT}%, ${ATTR_ALPHA})`;



            ctx.font = FONT_NAME;

            ctx.fillStyle = bg;
            ctx.fillRect(0,0, W, H);

            ctx.fillStyle = gridColor;
            
            ctx.fillRect(AV_RECT_X, AV_RECT_Y, AV_RECT_W, AV_RECT_H);
            ctx.drawImage(avatar, PAD, PAD, AV_SIZE, AV_SIZE);
            
            ctx.fillStyle = text;

            ctx.beginPath();

            ctx.font = FONT_DND;
            ctx.textBaseline = 'hanging';

            [
                `name: ${name}`,
                `class: ${clss}`,
                `fursona: ${fursona}`,
                `inventory: ${item1.split(",")[0]} ` + (Math.random() >0.2 ? `, ${item2.split(",")[0]}` : ""),
                `good at: ${skill1}`,
                `bad at: ${skill2}`,
                `secret power: ${power}`,
                `signature sex move: ${spell}`,
            ].forEach((line,i) => {
                ctx.fillText(line, AV_SIZE + PAD * 2, PAD + OFFSET_DND * i);
            })
                
            ctx.fillStyle = vibeColor;

            const special: [string, number][] = SPECIAL.map((label,i) => [label, clamp(id + i + SPEC_OFFSET)]);
            const vibes: [string, number][] = VIBES.map((label,i) => [label, clamp(id + i + VIBE_OFFSET)]);

            const vibeXY = (i, value) => {
                const phi = TAU / VIBES_N * i + VIBES_ROTATE;
                return [
                    VIBES_CX +  Math.cos(phi)  * value,
                    VIBES_CY +  Math.sin(phi)  * value
                ];
            }
            
            ctx.beginPath();
            
            vibes.forEach(([label, value], i) => {
                const [x,y] = vibeXY(i, VIBES_R_IN * value); 
                if (i === 0) {
                    ctx.moveTo(x,y);
                } else {
                    ctx.lineTo(x,y);
                }
            });
            
            ctx.closePath();
            
            ctx.fill();
            ctx.stroke();
            
            ctx.lineWidth = VIBES_GRID;
            ctx.strokeStyle = gridColor;
            vibes.forEach(([label, value], i) => {
                ctx.beginPath();
                const [x,y] = vibeXY(i, VIBES_R_IN); 
                ctx.moveTo(VIBES_CX, VIBES_CY);
                ctx.lineTo(x,y);
                ctx.stroke();
            });
            
            
            for (let j = 1; j <= GRID_NUM; j ++) {
                const MAX = j/GRID_NUM * VIBES_R_IN;
                ctx.beginPath();
                for (let i = 0; i < VIBES_N; i++) {
                    const [x,y] = vibeXY(i, MAX); 
                    if (i === 0) {
                        ctx.moveTo(x,y);
                    } else {
                        ctx.lineTo(x,y);
                    }
                };
                ctx.closePath(); 
                ctx.stroke();
            }

            ctx.lineWidth = VIBES_LINE;
            ctx.fillStyle = text;
            ctx.font = FONT_VIBES;
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            vibes.forEach(([label, value], i) => {
                const [x,y] = vibeXY(i, VIBES_R_MID);
                ctx.beginPath();
                ctx.fillText(label as string, x ,y + VIBE_LABEL_OFFSET);
                ctx.beginPath();
                const [px,py] = vibeXY(i, value * VIBES_R_IN);
                ctx.arc(px ,py, VIBE_POINT, 0, TAU);
                ctx.fill();
            });
            ctx.textBaseline = 'hanging';

            
            ctx.font = FONT_SPEC;
            special.forEach(([label, value], i ) => {
                const attr = value * ATTR_MAX;
                const y = PAD + PAD + TOP + i * (SPEC_ROW + SPEC_MARGIN);
                ctx.fillStyle = text;
                ctx.fillText(label, SPEC_LEFT, y);
                for (let j = 0; j <  ATTR_MAX; j ++) {
                    if (j > attr) {
                        ctx.fillStyle = noAttrColor;
                    } else {
                        ctx.fillStyle = attrColor;
                    }
                    ctx.fillRect(SPEC_BAR_LEFT + j * (SPEC_ROW + SPEC_PAD), y + SPEC_PAD, SPEC_RECT, SPEC_RECT);
                }

            });

            ctx.fillStyle = AUTH_LEFT;
            ctx.fillRect(COMPASS_LEFT, COMPASS_TOP, COMPASS_SQUARE, COMPASS_SQUARE);
            ctx.fillStyle = AUTH_RIGHT;
            ctx.fillRect(COMPASS_LEFT + COMPASS_SQUARE, COMPASS_TOP, COMPASS_SQUARE, COMPASS_SQUARE);
            ctx.fillStyle = LIB_LEFT;
            ctx.fillRect(COMPASS_LEFT, COMPASS_TOP + COMPASS_SQUARE, COMPASS_SQUARE, COMPASS_SQUARE);
            ctx.fillStyle = LIB_RIGHT;
            ctx.fillRect(COMPASS_LEFT + COMPASS_SQUARE, COMPASS_TOP + COMPASS_SQUARE, COMPASS_SQUARE, COMPASS_SQUARE);

            const px = rnd(id);
            const py = rnd(id + 5);

            
            let close = Infinity;
            let politics = "";

            ctx.fillStyle = text;
            ctx.font = POLITICS_FONT;

            
            TITLES.forEach(([x,y,label]) => {
                
                const dx = x - px;
                const dy = y - py;
                const distance = Math.sqrt(dx*dx+dy*dy);
                if (distance < close) {
                    close = distance;
                    politics = label;
                }
            });

            let yOffset = POLITICS_LINE;
            ctx.textBaseline = 'hanging';
            const lines = politics.split('\n');
            if (py > 0.5) {
                yOffset = -lines.length * POLITICS_LINE;
                ctx.textBaseline = "bottom";
            }

            lines.forEach((line, i) => {
                ctx.fillText(line, 
                    COMPASS_LEFT + (px * 0.9 + 0.05) * COMPASS_SIZE,
                    COMPASS_TOP + (py * 0.9 + 0.05) *COMPASS_SIZE + i * POLITICS_LINE + yOffset);
            });

            ctx.beginPath();
            ctx.arc(COMPASS_LEFT + px * COMPASS_SIZE, COMPASS_TOP + py * COMPASS_SIZE, 4,0, TAU);
            ctx.fill();

            ctx.textBaseline = "bottom";
            ctx.fillText("authoritarian", COMPASS_LEFT + COMPASS_SQUARE, COMPASS_TOP);
            ctx.textBaseline = "hanging";
            ctx.fillText("libertarian", COMPASS_LEFT + COMPASS_SQUARE, COMPASS_TOP + COMPASS_SIZE);
            
            
            ctx.save();
            ctx.translate(COMPASS_LEFT, COMPASS_TOP + COMPASS_SQUARE);
            ctx.rotate(-Math.PI/2);
            ctx.textBaseline = "bottom";
            ctx.fillText("left", 0, 0);
            ctx.translate(0, COMPASS_SIZE);
            ctx.textBaseline = "hanging";
            ctx.fillText("right", 0, 0);
            ctx.restore();

            const buffer = canvas.toBuffer();
            message.channel.send({files: [{attachment: buffer, name: 'vibecheck.png'}]}); 
        }
    }
];

module.exports = commands;