import { Message, MessageCollector, TextChannel, Guild, DMChannel, APIEmbed } from 'discord.js';
import { Command, Murderbot } from './../murderbot';
import { choice } from "../util";
const QUIZ_TIMEOUT = 1200000;
const CHAMPION = 'KING OF NERDS';
interface Question {
    question: string;
    answer: string;
    category: string;
}



const quizChampion = async (guild: Guild, mb: Murderbot) => {
    const result = await mb.getQuery(`select userID, points from (select userID, points from quiz_scores where guild='${guild.id}' order by points desc) limit 1`);
    return result;
}

async function quiz(message: Message) {
    console.log("RUNNING QUIZ");

        if(message.guild) {
            const previous = await this.getQuery(`SELECT answer from quiz_answers where guild = "${message.guild.id}"`);
            if (previous) {
                await message.channel.send(`W E A K\n the answer was\`\`\`\n${previous.answer}\`\`\``);
            }
        }
        const question: Question = choice(this.datasets.quiz);
        const answer = question.answer.toLowerCase().replace("\\", '');
        console.log(answer);
        try {
            await this.runQuery(`insert into quiz_answers values (${message.guild ? message.guild.id : message.author.id}, "${answer}")`);
        } catch (e) {
            await this.runQuery(`update quiz_answers set answer = "${answer.replace('"', '\"')}" where guild = "${message.guild.id}"`);
        }

        let embeds = [{
            color: 0xFF0000,
            title: question.category,
            description: question.question,
            fields: [
                {
                    name: 'Answer:', 
                    value: `\`\`\`css\n${answer.replace(/[a-z0-9]/ig, '*')} (${answer.length})\`\`\``
                }

            ]
        }];
        let sent = <Message>await message.channel.send({embeds});
        let collector = new MessageCollector(message.channel as TextChannel | DMChannel, {
            filter: m => m.content.toLowerCase() === answer,
        });
        const timesup = setTimeout(async () => {
                collector.stop();
                await this.runQuery(`delete from quiz_answers where guild = "${message.guild ? message.guild.id : message.author.id}"`);
                const embeds: APIEmbed[] = [{
                    title: "TIMES UP BITCHES",
                    description: question.question,
                    fields: [
                        {
                            name: 'answer was',
                            value:  `\`\`\`${answer}\`\`\``
                        }
                    ]
                }];
                sent.edit({embeds});
            }, QUIZ_TIMEOUT + answer.length * 2000);

        collector.on('collect', async (m: Message) => {
            collector.stop();
            let name = m.author.username;
            if (message.guild) {
                const score = await this.getQuery(`SELECT points from quiz_scores where userID = ${m.author.id} and guild = ${m.guild.id}`);
                if (!score) {
                    await this.runQuery(
                        `INSERT INTO quiz_scores VALUES (${m.author.id}, ${m.guild.id}, ${1})`);
                } else {
                    await this.runQuery(
                    `UPDATE quiz_scores SET points = ${score.points + 1} where userID = ${m.author.id} and guild = ${m.guild.id}`);
                }
                const member = await m.guild.members.fetch(m.author.id);
                if(member && member.nickname) {
                    name = member.nickname
                }
            }
            const embeds: APIEmbed[] = [{
                title: `${name} got this!`,
                description: question.question,
                fields: [
                    {
                        name: 'answer was',
                        value: `\`\`\`${answer}\`\`\``
                    }

                ]
            }];
            sent.edit({embeds});
            clearTimeout(timesup);
                // if (message.guild) {
                //     const champ = await m.guild.members.fetch((await quizChampion(m.guild, this)).userID);
                //     if(champ) {
                //         const role = await m.guild.roles.cache.find(r => r.name === CHAMPION);
                //         console.log(role, m.guild.roles.cache)
                //         if ( role) {
                //             const nerds = m.guild.members.cache.filter(m => m.roles.cache.has(role.id));
                //             for( let nerd of nerds.values()) {
                //                 await nerd.roles.remove(role);
                //             }
                //             const dethroned = nerds.first();
                //             await champ.roles.add(role);
                            
                //             if(dethroned && dethroned.id !== champ.id) {
                //                     await message.channel.send(`crowning the new NERDKING: ${champ.nickname || champ.user.username}`);
                //                 } 
                //         }else{
                //             console.log('Nerdking role is missing!');
                //         }
                //     }
                // }
                await m.channel.send(`${m.author} bingo! gj nerd.`);
                await this.runQuery(`delete from quiz_answers where guild = "${message.guild ? message.guild.id : message.author.id}"`);
                quiz.bind(this)(m);

            });
        }

const commands: Command[] = [
    {
        name: 'champ',
        help: 'quiz champ: reveals the person with the largest penis in the guild',
        regex: /^quiz\s+champi?o?n?$/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            if(!message.guild) {
                await message.channel.send('only works in guild chats. you shit');
                return;
            }
            
            const champ = await quizChampion(message.guild, this);
            if(champ) {
                const member = message.guild.members.cache.get(champ.userID);
                if(!member) {
                    throw new Error('something aint right with the quiz scores db');
                }
                const name = member.nickname || member.user.username;
                const points = champ.points;
                await message.channel.send(`${message.guild.name} quiz champion is ${name} with ${points} points.`);
            } else {
                await message.channel.send('no champion, no points, no hope');
            }
        }
    }, {
        name: 'purge',
        admin: true,
        regex: /^quiz\s+purge$/i,
        respond: async function(message: Message)  {
            try{
                await this.runQuery(`DELETE FROM quiz_scores where guild="${message.guild.id}"`);
                await message.channel.send(`quiz scores deleted for ${message.guild.name}`);  
            }catch(e) {
                console.log(e);
                throw new Error('Error purging quiz scores');
            }
        }
    }, {
        name: 'score',
        help: 'quiz score [<mention>]: jeopardy score',
        regex: /^quiz\s+score$/i,
        respond: async function(message: Message)  {
            let userID = message.author.id;
            let name = message.author.username;
            const mention = message.mentions.users.first();
            if(mention) {
                userID = mention.id;
                name = mention.username;
            }

            let score;
            try {
                score = await this.getQuery(`SELECT points FROM quiz_scores WHERE userID="${userID}" AND guild="${message.guild.id}"`);
            }catch(e){
                console.error(e);
                throw new Error('Error while reading quiz score from db');
            }
            if(score) {
                await message.channel.send(`${name} has ${score.points} points.`);
            }else{
                await message.channel.send(`${name} has exactly NO points. NONE. NADA. ZILCH.`);
            }
        }
    },{
        name: 'quiz',
        help: 'quiz: jeopardy questions.',
        regex: /^quiz$/i,
        respond: quiz
    },
    {
        name: "quiz scores",
        help: "quiz scores: score table for the server",
        regex: /^quiz +scores$/,
        respond: async function (message: Message) {
            let guildID = message.guild.id;
            let results = await this.allQuery(`select userID, points from quiz_scores where guild = "${guildID}"`);
            
            console.log(results);
            let table = ``;

            results.sort((a,b) => a.points >= b.points ? -1 : 1);

            let max = 10;

            const members = message.guild.members;
            const cache = members.cache;

            for (let i = 0, N = results.length; i < N; i++) {
                let {userID, points } = results[i];

                try {
                    let member = cache.has(userID) ? cache.get(userID) : await members.fetch(userID);
                    let name = member.nickname || member.user?.username;
                    let score = points + "";
                    let pad = new Array(max - score.length).fill('_').join("");
    
                    table += `${score}${pad}${name}\n`;
                } catch (e) {
                    console.error(e)
                    console.log("user id: ",userID)
                }

            }


            let msg = `
\`\`\`
${table}
\`\`\``
            message.channel.send(msg);
        }
     }

];
module.exports = commands;
