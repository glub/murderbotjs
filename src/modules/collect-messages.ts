import { Message } from 'discord.js';
import { Command, Murderbot } from './../murderbot';
const fs = require('fs').promises;

import {resolve} from 'path';
import { GuildChannel } from 'discord.js';
const commands: Command[] = [
    {
        name: 'collect',
        admin: true,
        regex: /^collect$/,
        respond: async function respond(message: Message) {


            if (message.channel.isDMBased()) {
                message.channel.send('not in dms');
                return;
            }
            const channelName = (message.channel as GuildChannel).name;

            let last = message;
            const collected = {};
            let N = 0;
            let name = message.guild.name;
            let iteration = 0;

            const COLLECTING = "collecting...";

            const collector = message.channel.createMessageCollector({
                filter: m => m.author.id === message.author.id && m.content === "stop collecting", max: 1
            });

            let stop = false;
            collector.on('end', _ => {
                stop = true;
                feedback.edit(`Stopped collecting, collected ${N} messages`);
            });

            const feedback = await message.channel.send(COLLECTING);
            while(last && !stop) {
                let messages = await message.channel.messages.fetch({
                    limit: 100,
                    before: last.id
                });
                last = messages.at(messages.size - 1);
                N += messages.size;
                messages.forEach((m: Message) => {
                    collected[m.author.username] = collected[m.author.username] || [];
                    collected[m.author.username].push(m.content);

                });
                iteration++;

                if (iteration >10) {
                    iteration = 0;
                    await fs.writeFile(resolve(__dirname, `../../${name}-${channelName}.json`),JSON.stringify(collected),
                    {
                        encoding: 'utf-8'
                    });
                    await feedback.edit(`${COLLECTING}${N} messages collected`)
                }
            }
            if (!stop) {
                await feedback.edit(`finished collecting, collected ${N} messages`);
            }
        }
    }
]

module.exports = commands;