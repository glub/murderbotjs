import { Message, MessageCollector } from "discord.js";
import { inspect } from "util";
import { Command } from "../murderbot";




const getEmbeds = (clue, answer) => [
         {
            color: 0xFFFF55,
            title: clue,
            fields: [
                {
                   name: 'answer',
                   value: '```' + answer + '```'
                }
            ]

        }
    ];

const commands: Command[] = [
    {
        name: 'clue',
        help: 'get a cryptic clue from times crosswords',
        regex: /^clue$/,
        respond: async function(message: Message, match: RegExpMatchArray)  {

            const db = this.datasets.clues;
            let {clue, answer, num} = await db.get('select row_number() over() as num, clue, answer from clues_fts order by random() limit 1');
            const charades = await db.all(`select charade, answer from charades_by_clue where clue_rowid = ${num}`);
            const explanation = charades.map(({charade, answer}) => charade + ': ' + answer).join(', ');
            answer = answer.toUpperCase();
            let hidden = answer.replace(/\S/g, '█');
            let encoded = hidden;

            const question = await message.channel.send({embeds: getEmbeds(clue, encoded)});

            const collector = new MessageCollector(message.channel, {
                filter: msg => msg.content.replace(/\S/g, '█') === hidden
            });

            const win = async msg => {
                collector.stop();
                let name = msg.author.username;
                        const member = await msg.guild?.members.fetch(msg.author.id);
                        name = member?.nickname || name;

                        const embeds = [{
                            color: 0x55FF55,
                            title: clue,
                            description: name + ' got it',
                            fields: [
                                {
                                    name: 'answer', value: answer
                                }
                            ]
                        }];
                        if (explanation) {
                            embeds[0].fields.push({name:'breakdown', value: explanation});
                        }

                        await question.edit({embeds});
            };


            collector.on('collect', async msg => {

                const attempt = msg.content.toUpperCase();

                if (attempt === answer) {
                    win(msg);
                } else {
                    let temp = '';
                    for (let i=0,N=answer.length;i<N;i++) {
                        if (attempt[i] === answer[i]) {
                            temp += answer[i];
                        } else {
                            temp += encoded[i];
                        }
                    }

                    encoded = temp;
                    if (encoded === answer) {
                        win(msg);
                    } else {
                        question.edit({embeds: getEmbeds(clue, encoded)});
                    }
                    console.log(msg.content);
                }


            });


            const timesup = setTimeout(() => {
                collector.stop();
                const embeds = [{
                    color: 0xFFAAAA,
                    title: clue,
                    description: 'no one got it',
                    fields: [{
                        name: 'answer',
                        value: answer
                    }],
                }];
                if (explanation) {
                    embeds[0].fields.push({name: 'breakdown', value: explanation});
                }
                question.edit({embeds});
                
            }, 5 * 3600000);

        }
    }
];

module.exports = commands;