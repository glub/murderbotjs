import { promisify } from 'util';
import { Message } from 'discord.js';
import { Command, Murderbot } from './../murderbot';

const COPTERS = [
`\`\`\`
 ROFL:ROFL:LOL: 
         ___^___ _
      __/      [] \\    
LOL===__           \\ 
        \\___ ___ ___]
              I   I
          ----------/
^ ^^ ^ ^ ^^ ^ ^ ^^ ^ ^ ^^ ^ 
\`\`\``,
`\`\`\`
          :LOL:ROFL:ROFL
         ___^___ _
 L    __/      [] \\    
 O ===__           \\ 
 L      \\___ ___ ___]
              I   I
          ----------/
 ^^ ^ ^ ^^ ^ ^ ^^ ^ ^ ^^ ^ ^
\`\`\``,
`\`\`\`
 ROFL:ROFL:LOL: 
         ___^___ _
      __/      [] \\    
LOL===__           \\ 
        \\___ ___ ___]
              I   I
          ----------/
^^ ^ ^ ^^ ^ ^ ^^ ^ ^ ^^ ^ ^ 
\`\`\``,
`\`\`\`
          :LOL:ROFL:ROFL
         ___^___ _
 L    __/      [] \\    
 O ===__           \\ 
 L      \\___ ___ ___]
              I   I
          ----------/
^ ^ ^ ^^ ^ ^ ^^ ^ ^ ^^ ^ ^ ^
\`\`\``,
`\`\`\`
 ROFL:ROFL:LOL: 
         ___^___ _
      __/      [] \\    
LOL===__           \\ 
        \\___ ___ ___]
              I   I
          ----------/
 ^ ^ ^^ ^ ^ ^^ ^ ^ ^^ ^ ^ ^^
\`\`\``,
`\`\`\`
          :LOL:ROFL:ROFL
         ___^___ _
 L    __/      [] \\    
 O ===__           \\ 
 L      \\___ ___ ___]
              I   I
          ----------/
^ ^ ^^ ^ ^ ^^ ^ ^ ^^ ^ ^ ^^ 
\`\`\``
];
const CRASH = 
`\`\`\`
    ROFLCOPTER CRASHED RIPPERONI
         ___<_/_F_
 L....__/ \\  R  [// \\...ЯOF... L
\`\`\``;
const OWO = [
    `|                                   (owo)`,
    `|                                  (owo)`,
    `|                                 (owo)`,
    `|                                (owo)`,
    `|                               (owo)`,
    `|                              (owo)`,
    `|                             (owo)`,
    `|                            (owo)`,
    `|                           (owo)`,
    `|                          (owo)`,
    `|                         (owo)`,
    `|                        (owo)`,
    `|                       (owo)`,
    `|                      (owo)`,
    `|                     (owo)`,
    `|                    (owo)`,
    `|                   (owo)`,
    `|                  (owo)`,
    `|                 (owo)`,
    `|                (owo)`,
    `|               (owo)`,
    `|              (owo)`,
    `|             (owo)`,
    `|            (owo)`,
    `|           (owo)`,
    `|          (owo)`,
    `|         (owo)`,
    `|        (owo)`,
    `|       (owo)`,
    `|      (owo)`,
    `|     (owo)`,
    `|    (owo)`,
    `|   (owo)`,
    `|  (owo)`,
    `| (owo)`,
    `|(owo)`,
    `|owo)`,
    `|wo)`,
    `|o)`,
    `|)`,
    `|      he gone....`
];


const CHOMP = [
`\`\`\`
                                
              /    o     o    
oh no!       |  /\\/\\/\\/\\/\\/\\  
    \\o/       \\  \\/\\/\\/\\/\\/\\/ 
\`\`\``,
`\`\`\`
              /    o     o    
             |  /\\/\\/\\/\\/\\/\\  
oh no!                        
    \\o/       \\  \\/\\/\\/\\/\\/\\/ 

\`\`\``,
`\`\`\`
                                     
             /    o     o    
oh no!      |  /\\/\\/\\/\\/\\/\\  
    \\o/      \\  \\/\\/\\/\\/\\/\\/ 
\`\`\``,
`\`\`\`
             /    o     o    
            |  /\\/\\/\\/\\/\\/\\  
oh no!                        
    \\o/      \\  \\/\\/\\/\\/\\/\\/ 

\`\`\``,
`\`\`\`
                                    
            /    o     o    
oh no!     |  /\\/\\/\\/\\/\\/\\  
    \\o/     \\  \\/\\/\\/\\/\\/\\/ 
\`\`\``,
`\`\`\`
            /    o     o    
           |  /\\/\\/\\/\\/\\/\\  
oh no!                       
    \\o/     \\  \\/\\/\\/\\/\\/\\/ 

\`\`\``,
`\`\`\`
                                     
           /    o     o    
oh no!    |  /\\/\\/\\/\\/\\/\\  
    \\o/    \\  \\/\\/\\/\\/\\/\\/ 
\`\`\``,
`\`\`\`
           /    o     o    
          |  /\\/\\/\\/\\/\\/\\  
oh no!                       
    \\o/    \\  \\/\\/\\/\\/\\/\\/ 

\`\`\``,
`\`\`\`
                                       
          /    o     o    
oh no!   |  /\\/\\/\\/\\/\\/\\  
    \\o/   \\  \\/\\/\\/\\/\\/\\/ 
\`\`\``,
`\`\`\`
          /    o     o    
         |  /\\/\\/\\/\\/\\/\\  
oh no!                       
    \\o/   \\  \\/\\/\\/\\/\\/\\/ 

\`\`\``,
`\`\`\`
                            
         /    o     o    
oh no!  |  /\\/\\/\\/\\/\\/\\  
    \\o/  \\  \\/\\/\\/\\/\\/\\/ 
\`\`\``,
`\`\`\`
         /    o     o    
        |  /\\/\\/\\/\\/\\/\\  
oh no!                       
    \\o/  \\  \\/\\/\\/\\/\\/\\/ 

\`\`\``,
`\`\`\`
                               
        /    o     o    
oh no! |  /\\/\\/\\/\\/\\/\\  
    \\o/ \\  \\/\\/\\/\\/\\/\\/ 
\`\`\``,
`\`\`\`
        /    o     o    
       |  /\\/\\/\\/\\/\\/\\  
oh no!                       
    \\o/ \\  \\/\\/\\/\\/\\/\\/ 

\`\`\``,
`\`\`\`
                            
       /    o     o    
oh no!|  /\\/\\/\\/\\/\\/\\  
    \\o/\\  \\/\\/\\/\\/\\/\\/ 
\`\`\``,
`\`\`\`
       /    o     o    
      |  /\\/\\/\\/\\/\\/\\  
oh no!                       
    \\o/\\  \\/\\/\\/\\/\\/\\/ 

\`\`\``,
`\`\`\`
     /    o     o    
    |  /\\/\\/\\/\\/\\/\\  
         \\o/              
    \\  \\/\\/\\/\\/\\/\\/ 
\`\`\``,
];

const CHOMP_YES = 
`\`\`\`

     /    o     o    
    |  /\\/\\/\\/\\/\\/\\  
    \\  \\/\\/\\/\\/\\/\\/ 
\`\`\``;
const CHOMP_NEITHER = 
`\`\`\`
 ??? /    o     o    
    |  /\\/\\/\\/\\/\\/\\  
         \\o/ ???
    \\  \\/\\/\\/\\/\\/\\/ 
\`\`\``;

const CHOMP_NO = 
`\`\`\`
       
            |
            |    /    X     X   
         \\o/+   |  /\\/\\/\\/\\/\\/\\          
    \\  \\/\\/\\/\\/\\/\\/ 
\`\`\``

const DELAY = 1200;
const ITERATIONS = 100;
const request =  promisify(require('request'));
const keys = require('../../keys.js');
const COPTER_DURATION = 50;
const commands: Command[] = [
    {
        name: 'hide owo',
        help: 'hide owo: self-explanatory',
        regex: /^hide\s+owo$/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const m = (<Message> await message.channel.send(OWO[0]));
            await rofl(m, 0, OWO);
        }
    },
    {
        name: 'roflcopter',
        help: 'roflcopter: rofl rofl',
        regex: /^roflcopter$/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const m = (<Message> await message.channel.send(COPTERS[0]));
            await rofl(m, 0, COPTERS, COPTER_DURATION, async () => {
                await m.edit(CRASH);
            });
        }
    },
    // {
    //     name: 'chomp',
    //     help: 'chomp <whatever>: chomp (or not)',
    //     regex: /^chomp\s+(.+)$/i,
    //     respond: async function(message: Message, match: RegExpMatchArray)  {
    //         const thing = match[1];
    //         const label = (<Message> await message.channel.send(`Chomp ${thing}? Or not?`));
    //         let error  =false;
    //         try {
    //             await label.react('👍');
    //             await label.react('👎');

    //         } catch(e) {
    //             error = true;
    //         }
    //         const chomp = (<Message> await message.channel.send(CHOMP[0]));
    //         await rofl(chomp, 0, CHOMP, 0, async () => {
    //             let result = `not allowed to add emojis on this server. so no polling, only chomping.`;
    //             let frame = CHOMP_YES;
    //             if(!error) {
    //                 const yes = label.reactions.find(r => r.emoji.name === '👍').users.size - 1;
    //                 const no = label.reactions.find(r => r.emoji.name === '👎').users.size - 1;
    //                 result = yes === no ? `It's a draw! you indecisive wimps` : (
                        
    //                     yes > no ? `${thing} was chomped` : `${thing} lives!`);
    //                 frame = yes === no ? CHOMP_NEITHER : (yes > no ? CHOMP_YES : CHOMP_NO);
    //             }
    //             await chomp.edit(frame);
    //             await message.channel.send(result);

    //         });
    //     }
    // }
];
module.exports = commands;

async function rofl(message: Message, i: number, frames: string[], duration?: number, finish?: () => void): Promise<any> {
        let dur = duration || frames.length;
        setTimeout(async () => {
            i++;
            if(i < dur) {
                await message.edit(frames[i%frames.length]);
                rofl(message, i, frames, dur, finish);
            } else if(finish){
                finish();
            }
        }, DELAY);
}