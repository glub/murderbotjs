// require("canvas-webp");
import { GuildMember, Message, MessageReaction, User } from 'discord.js';
import { exists, readFile } from 'fs';
import { resolve } from 'path';
import { promisify } from 'util';
import { Command } from './../murderbot';
const  { createCanvas, loadImage, Image, registerFont } = require('canvas');
const readFileAsync = promisify(readFile);
const existsAsync = promisify(exists);
const keys = require('../../keys.js');
const BG = '#36393e';
const SIZE = 1024;
const CENTER = SIZE * 0.5;
const ARR_ANGLE = Math.PI/8;
const ARR_SIZE = SIZE/80;
const request = promisify(require('request'));

const USERPIC_R = SIZE*0.03;
const USER_RADIUS = SIZE * 0.4;

const ARR_RADIUS = USER_RADIUS - USERPIC_R;



const L_X = SIZE / 100;
const L_Y = SIZE / 100;
const L_WIDTH = SIZE / 20;
const L_HEIGHT = SIZE / 200;
const L_OFFSET = SIZE / 50;

const L_TEXT_X = L_X * 2 + L_WIDTH;

const ARROW_SPAN = USERPIC_R / ARR_RADIUS  

const COLORS = [
    'black',
    '#6495ED',
    '#FF1493',
    'orange',
    'red'
];

const LABELS  = [
    'Bantz',
    'Tsundere Bantz',
    'Tsundere',
    'Tsundere Love',
    'Love'
];

interface Arrow {
    userFrom: string;
    userTo: string;
    sentiment: number;
    from: {
        who: Entity,
        x: number,
        y: number
    };
    to: {
        who: Entity,
        x: number,
        y: number
    };
    color?: string;
}


class Entity {
    from: Arrow[] = [];
    to: Arrow[] = [];
    angle: number = 0;
    index: number = 0;
    x: number = 0;
    y: number = 0;
    constructor(public member: GuildMember) {
    }
    setPosition(index: number, L: number): void {
        this.index = index;
        this.angle = (Math.PI * 2) * index / L;
        this.x = Math.cos(this.angle) * USER_RADIUS + CENTER;
        this.y = Math.sin(this.angle) * USER_RADIUS + CENTER;
    }
}


const commands: Command[] = [
    {
        name: 'shipping chart',
        help: 'shipping chart: hot gossip',
        regex: /^shipping\s+chart.*$/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const guild = message.guild;
            if(!guild) {
                await message.channel.send('can only be used in guild chats')
                return;
            }
            const data = await generateData.bind(this)(message);
            if(!data) {
                await message.channel.send('no shipping data for this guild');
                return;
            }
            const {users, arrows} = data;
            if (arrows.length === 0) {
                await message.channel.send('no data for this chat');
                return;
            }
            registerFont(resolve(__dirname, '../../resources/Roboto.ttf'), {family: 'Roboto'});

            const canvas = createCanvas(SIZE, SIZE);
            const ctx = canvas.getContext('2d');
            ctx.clearRect(0,0,SIZE, SIZE);
            const loading = await message.channel.send('Processing...');
            const avatars = await getAvatars.bind(this)(ctx, canvas, users);
            ctx.restore();
            ctx.fillStyle = BG;
            ctx.fillRect(0,0,SIZE, SIZE);
            drawLegend(ctx);
            drawArrows(ctx, arrows);
            drawArrowHeads(ctx, arrows);
            
            avatars.forEach(avatar => {
                ctx.drawImage(avatar, 0,0,SIZE,SIZE);
            });
            const buffer = canvas.toBuffer();
            await (loading as Message).delete();
            await message.channel.send({files:[{attachment: buffer, name: 'chart.png'}]});
        }
    }

];
module.exports = commands;

async function generateData(message: Message): Promise<{users: Map<string, Entity>, arrows: Arrow[]} | null>{
    const guild = message.guild;
    let ids = Array.from(message.mentions.users.values()).map(u => u.id);
    const sql = 
    `SELECT userFrom, userTo, sentiment FROM sentiment 
            WHERE guild="${guild.id}"
            ${ids.length > 0 ?`AND (
            userFrom in (${ids.map(id => `"${id}"`).join(', ')})
            OR userTo in (${ids.map(id => `"${id}"`).join(', ')})
        )`:''}`;
    let arrows: Arrow[] = await this.allQuery(sql);
        if(!arrows){
            return null;
        }
        const users = new Map();

        const missing = [];
        for(let i = 0, N = arrows.length; i < N; i++) {
            let arrow = arrows[i];
            if (missing.indexOf(arrow.userFrom) > -1 || missing.indexOf(arrow.userTo) > -1) {
                arrows[i] = null;
                continue;
            }
            let cache = guild.members.cache;
            let memberFrom: GuildMember;
            let memberTo: GuildMember;
            
            try {
                memberFrom = cache.has(arrow.userFrom) ? cache.get(arrow.userFrom) : await guild.members.fetch(arrow.userFrom);
            } catch(e) {
                missing.push(arrow.userFrom);
                arrows[i] = null;
                continue;
            }
            
            try {
                memberTo = cache.has(arrow.userTo) ? cache.get(arrow.userTo) : await guild.members.fetch(arrow.userTo);
            } catch(e) {
                missing.push(arrow.userTo);
                arrows[i] = null;
                continue;

            }
            if(!users.has(arrow.userFrom)) {
            users.set(arrow.userFrom, new Entity(memberFrom));
            }
            
            if(!users.has(arrow.userTo)) {
            users.set(arrow.userTo, new Entity(memberTo));
            }
            const eFrom = users.get(arrow.userFrom);
            eFrom.from.push(arrow);
            const eTo = users.get(arrow.userTo);
            eTo.to.push(arrow);
            arrow.from = { who: eFrom, x:0, y:0};
            arrow.to = { who: eTo, x:0, y:0};
        };
    arrows = arrows.filter(a => !!a);
    const L = users.size;
    let index = 0;
    users.forEach((user, id) => {
        user.setPosition(index, L);
        index++;
    });
    arrows.forEach(arrow => {
        const str = Math.floor((arrow.sentiment + 1)*2.4999);
        console.log(str, arrow.sentiment)
        arrow.color = COLORS[str];

        const fromArray = arrow.from.who.from;
        const toArray = arrow.to.who.to;
        const lFrom = fromArray.length + arrow.from.who.to.length;
        const lTo = toArray.length + arrow.to.who.from.length;


        const iFrom = fromArray.indexOf(arrow) - lFrom / 2 + 0.5;
        const iTo = toArray.indexOf(arrow) + arrow.to.who.from.length - lTo / 2 + 0.5;

        const angleFrom = ARROW_SPAN  * iFrom / lFrom + arrow.from.who.angle;
        const angleTo = ARROW_SPAN * iTo / lTo + arrow.to.who.angle;


        arrow.from.x = Math.cos(angleFrom)* ARR_RADIUS + CENTER;
        arrow.from.y = Math.sin(angleFrom)* ARR_RADIUS + CENTER;
        arrow.to.x = Math.cos(angleTo)* ARR_RADIUS + CENTER;
        arrow.to.y =Math.sin(angleTo)* ARR_RADIUS + CENTER;
        
    });
    return {users, arrows};
}
function drawArrows(ctx: any, arrows: Arrow[]): void {
    arrows.forEach(arrow => {
        ctx.strokeStyle = arrow.color;
        ctx.beginPath();
        ctx.moveTo(arrow.from.x, arrow.from.y);
        ctx.quadraticCurveTo(CENTER,CENTER, arrow.to.x, arrow.to.y);
        ctx.stroke();    
    });
}

async function getAvatars(ctx: any, canvas: any, users: Map<string, Entity>): Promise<any[]> {
    const avatars: any[] = [];
    ctx.fillStyle = 'black';
    for (let user of users.values()) {
        if (user.member) {
            ctx.save();
            ctx.beginPath();
            ctx.arc(user.x, user.y, USERPIC_R, 0, Math.PI * 2);
            ctx.fill();
            ctx.clip();
            let avatar = await loadImage(user.member.user.displayAvatarURL({extension: 'jpg'}));
            ctx.drawImage(avatar, user.x - USERPIC_R, user.y - USERPIC_R, USERPIC_R * 2, USERPIC_R * 2);
            const avaBuffer = canvas.toBuffer();
            const pic = new Image();
            pic.src = avaBuffer;
            avatars.push(pic);
            ctx.restore();
            ctx.clearRect(0, 0, SIZE, SIZE);
        } else {
            console.log(user);
        }
    };
    return avatars;
}

function drawArrowHeads(ctx: any, arrows: Arrow[]): void {
    arrows
        .forEach(arrow => {
            ctx.beginPath();
            ctx.strokeStyle = arrow.color;
            ctx.fillStyle = arrow.color;
            ctx.moveTo(arrow.from.x, arrow.from.y);
            ctx.quadraticCurveTo(CENTER,CENTER, arrow.to.x, arrow.to.y);
            ctx.stroke();    

            ctx.beginPath();
            ctx.moveTo(arrow.to.x, arrow.to.y);
            ctx.lineTo(
                arrow.to.x
                    - ARR_SIZE*Math.cos(
                        Math.atan2(
                            arrow.to.y-CENTER, arrow.to.x-CENTER
                            ) + ARR_ANGLE
                        ),
                arrow.to.y
                    - ARR_SIZE*Math.sin(
                        Math.atan2(
                            arrow.to.y-CENTER, arrow.to.x-CENTER
                            ) + ARR_ANGLE
                        )
                );
            ctx.lineTo(
                arrow.to.x
                    - ARR_SIZE*Math.cos(
                        Math.atan2(
                            arrow.to.y-CENTER, arrow.to.x-CENTER
                            ) - ARR_ANGLE
                        ),
                arrow.to.y
                    - ARR_SIZE*Math.sin(
                        Math.atan2(
                            arrow.to.y-CENTER, arrow.to.x-CENTER
                            ) - ARR_ANGLE
                        )
                );
            ctx.closePath();
            ctx.fill();
            
    });
}


function drawLegend(ctx: any) {
    ctx.textAlign = 'start';
        ctx.font = "11px Roboto";
        ctx.textBaseline = 'middle';
        COLORS
            .forEach((color, index) => {
                ctx.beginPath();
                ctx.rect(L_X, L_Y + L_OFFSET * (index + 0.5), L_WIDTH, L_HEIGHT);
                ctx.fillStyle = color;
                ctx.fill();
                ctx.beginPath();
                ctx.fillStyle = 'white';
                ctx.fillText(LABELS[index], L_TEXT_X, L_Y + L_OFFSET * (index + 0.5));
            });
}