import { Command } from "../murderbot";
import { Message } from "discord.js";
import { choice } from "../util";

const commands: Command[] = [
    {
        regex: /^how +(.+)/i,
        name: 'wikihow',
        help: 'how [anything]: clear and helpful instructions',
        respond: async function(message: Message, match: RegExpMatchArray) {
            const steps = [1,2,3].map(n => `${n}. ${choice(this.datasets.wikihow)}`)
            await message.channel.send(steps.join('\n'));

        }
    }
];

module.exports = commands;