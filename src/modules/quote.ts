import { Canvas, Image } from "canvas";
import { ChannelType, Client, Guild, GuildMember, Message, TextChannel, User } from "discord.js";
import { resolve } from "path";
import { Command } from "../murderbot";
import { choice, getPicURL, getPicURLSingle } from "../util";

const MONTHS = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
];

const {createCanvas, loadImage, registerFont} = require('canvas');

const W = 960;
const H = 480;
const PAD = 10;
const FRAME_LINE = 2;
const QUOTE_W = W - 4 * PAD;

const BG = 'black';
const FONT = 'white';
const DATE = '#FFFF00CC';
const FRAME = 'yellow';

const commands: Command[] = [
    {
        regex: /^add +quote( +(<@\d+>) *(.+))?/,
        name: 'add quote',
        help: 'add quote (while replying) or quote <mention> <text>: add to quotes',
        respond: async function(message: Message, match: RegExpMatchArray) {
            if (!message.guild) {
                return;
            }

            const mention = message.mentions?.users?.first();

            let user: User;
            let quote: string;
            let timestamp: number;

            if (message.reference) {
                const answerTo = await message.channel.messages.resolve(message.reference.messageId);
                user = answerTo.author;
                quote = answerTo.content;
                timestamp = answerTo.createdTimestamp;
                if (!answerTo.content) {
                    await message.channel.send("no text in that message.\ndumbass.");
                    return;
                }
            } else{
                if (!match[1]) {
                    
                    await message.channel.send("what quote?? either reply to a text post or do add quote @user quote");
                    return;
                }

                user = mention;
                quote = match[3];
                timestamp = message.createdTimestamp;
            }

            

            const member = message.guild.members.resolve(user.id);

            const id = message.guildId;
            
            const date = new Date(timestamp);
            let dateString;

            if (message.reference) {
                dateString = `${date.getDate()} ${MONTHS[date.getMonth()]}, ${date.getFullYear()}`;
            } else{
                dateString = `circa ${MONTHS[date.getMonth()]} ${date.getFullYear()}`;
            }

            const settings = await this.getQuery(`select quoteChannel from settings where guild = "${id}"`)

            this.runQuery(`insert into quotes(guildID, userID, quote, date) values("${id}", "${user.id}", '${(quote.replace(/'/g, "''"))}', "${dateString}")`);
            
            let channel: TextChannel;
            console.log(settings);
            if (!settings?.quoteChannel) {
                channel = message.channel as TextChannel;
            } else {
                const client: Client = this.client;
                channel = (client.channels.resolve(settings.quoteChannel) as TextChannel);
                message.channel.send(`posting in #<#${channel.id}>`);
            }
            
            const canvas = await drawQuote(member, quote, dateString);
            const buffer = canvas.toBuffer();
            channel.send({files: [{attachment: buffer, name: 'quote.png'}]}); 

        }
    }, {
        regex: /^quote channel (<#(\d+)>|reset)/,
        name: 'quote channel',
        help: 'quote channel <#channel-name or reset>: set or reset the channel for posting quotes',
        respond: async function(message: Message, match: RegExpMatchArray) {
            if (!message.guild) {
                return;
            }
            const id = message.guildId;
            const channelId = match[2] || null;
            if (channelId) {
                const client: Client = this.client;
                const channel = client.channels.cache.get(channelId);
                
                if (channel.type !== ChannelType.GuildText && channel.type !== ChannelType.GroupDM) {
                    await message.channel.send("that's not a text channel you fool");
                    return;
                }
            }
            



            await this.runQuery(`insert or ignore into settings(guild) values("${id}")`);
            await this.runQuery(`update settings set quoteChannel = "${channelId}" where guild = "${id}"`);
            await message.channel.send("aight");
        }
    }, {
        name: 'quote',
        help: 'quote [@mention]: get a random quote',
        regex: /^quote( <@\d+>)?$/i,
        respond: async function(message: Message, match: RegExpMatchArray) {
            const mention = message.mentions?.members?.first()?.id;
            const quotes: {userID: string, quote: string, date: string}[] = await this.allQuery(`select * from quotes where guildID = "${message.guildId}"` +  (mention ? ` and userID = "${mention}"`: ""));
            if (!quotes.length) {
                message.channel.send("no results.");
                return;
            } 

            const {userID, quote, date} = choice(quotes);
            
            const member = await message.guild.members.fetch(userID);
            const canvas = await drawQuote(member,  quote.replace(/''/g, "'"), date);

            const buffer = canvas.toBuffer();
            message.channel.send({files: [{attachment: buffer, name: 'quote.png'}]}); 
        }
    }

]


async function drawQuote(member: GuildMember, quote: string, date: string): Promise<any> {

    const nameSize = 20;
    const nameX = W - PAD * 2;
    const nameY = H - PAD * 4;

    const dateSize = 13;
    const dateX = W - PAD * 2;
    const dateY = H - PAD*2;

    const quoteW = W - 10 * PAD;
    const quoteH = H - 10 * PAD - nameSize;

    const quoteRatio = quoteW/quoteH;



    const avatar = await loadImage(member.displayAvatarURL({extension: 'jpg'}));
    const name = member.nickname || member.user.username;
    registerFont(resolve(__dirname, '../../resources/Roboto-Italic.ttf'), {family: 'Roboto Italic'});
    registerFont(resolve(__dirname, '../../resources/Cookie-Regular.ttf'), {family: 'Cookie'});

    const canvas= createCanvas(W,H);
    const ctx: CanvasRenderingContext2D = canvas.getContext('2d');
    ctx.fillStyle = BG;
    ctx.fillRect(0,0,W,H);
    ctx.globalAlpha = 0.2;
    ctx.drawImage(avatar,0,(H-W)/2, W, W);
    ctx.globalAlpha = 1;

    ctx.fillStyle = FONT;

    let quoteSize = 60;
    ctx.font = quoteSize +"px Cookie";

    quote = `“${quote}”`;

    const size = ctx.measureText(quote);

    const ratio = size.width / quoteSize;

    const linesN = Math.ceil (ratio / quoteRatio / 9);

    const lineW = Math.max(quoteW, size.width / linesN);

    
    


    const words = quote.split(/ +/);

    const lines = [];

    let maxLineW = -Infinity;
    let line = "";
    words.forEach((word,i) => {
        line += word;
        const lineSize = ctx.measureText(line);
        // const next = i < words.length - 1 ? ctx.measureText(words[i + 1] + " ").width : 0;
        const next = 0;
        if (lineSize.width + next >= lineW || i >= words.length - 1) {
            lines.push(line);
            maxLineW = Math.max(maxLineW, lineSize.width);
            line = "";
        } else {
            line += " ";
        }
    });

    const mult = Math.min(1, quoteW / maxLineW);
    quoteSize = quoteSize * mult;
    console.log("qw:", quoteW, "size width:", size.width, "maxLineW:", maxLineW)
    console.log("linesN:", linesN, "linesW:",lineW, "ratio:", ratio, "quoteRatio:", quoteRatio, "quoteSize:",quoteSize, mult)

    ctx.textAlign = "start";

    let quoteX = PAD * 5;
    if (lines.length === 1) {
        ctx.textAlign = "center";
        quoteX = W / 2;
        
    }
    ctx.textBaseline = "hanging";

    const y0 = Math.max(0, quoteH - lines.length * quoteSize) / 2 + PAD * 4;
    ctx.font = quoteSize +"px Cookie";
    lines.forEach((line, i) => {

        
        const quoteY = y0 + i * quoteSize;
        ctx.fillText(line, quoteX, quoteY);
    })


    

    ctx.textBaseline = "bottom";

    ctx.textAlign = "end";
    ctx.font = nameSize +"px Roboto Italic";
    
    
    ctx.fillText(`-${name}`, nameX, nameY);

    ctx.font = dateSize +"px Roboto Italic";
    
    ctx.fillStyle = DATE;
    

    ctx.fillText(date, dateX, dateY);


    ctx.strokeStyle = FRAME;
    ctx.lineWidth = FRAME_LINE;
    ctx.strokeRect(PAD, PAD, W-PAD*2, H-PAD*2);
    return canvas;
}

module.exports = commands;