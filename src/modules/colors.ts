import { APIEmbed, HexColorString, Message } from 'discord.js';
import { Command } from './../murderbot';
import { promisify } from 'util';
const request = promisify(require('request'));
const URL = 'http://www.colourlovers.com/api/';

interface ColorInfo {
    id: number;
    title: string;
    hex: string;
    rgb: {red:number, green:number, blue:number};
    hsv: {hue: number, saturation:number, value: number};
    description: string;
    imageUrl: string;
}

const commands: Command[] = [
    {
        name: 'color by keywords',
        help: 'what is the color of <whatever>: color info',
        regex: /^what('?s|\s+is)\s+the\s+color\s+of\s+(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const query = encodeURI(match[2]);
            const url = URL + `/colors?keywords=${query}&format=json`;
            const response = await request(url);
            const colors: ColorInfo[] = JSON.parse(response.body);
            const index = (colors.length * Math.random()) | 0;
            const color = colors[index];
            if(!color) {
                await message.channel.send('None, apparently');
                return;
            }
            await message.channel.send({embeds: [colorInfo(color)]});

        }
    }, {
        name: 'hex color',
        help: 'color #<hex color>: color info',
        regex: /^color\s+#?([a-f0-9]{6})$/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const hex = match[1];
            const url = URL + `/color/${hex}?format=json`
            const response = await request(url);
            const color: ColorInfo = JSON.parse(response.body)[0];
            await message.channel.send({
                embeds: [colorInfo(color)]
            });
        }
    }

];
module.exports = commands;

function colorInfo(color: ColorInfo): APIEmbed {
    const {red, green, blue} = color.rgb;
    const {hue, saturation, value} = color.hsv;
    return {
        color: parseInt(color.hex, 16),
        title: color.title,
        fields: [
            {
                name: 'RGB',
                value: `${red}, ${green}, ${blue}`
            },
            {
                name: 'HSV',
                value: `${hue}, ${saturation}, ${value}`
            },
            {
                name: 'HEX',
                value: color.hex
            },
        ],
        image: {
            url: color.imageUrl
        }
    }
}