import { Message } from 'discord.js';
import { Command, Murderbot } from './../murderbot';
import {
    antonyms
} from '../dictionary-util';
import { rnd } from '../util';
const commands: Command[] = [
    {
        name: 'how',
        priority: 5,
        help: 'how <something> is <something>: precise measure of all things',
        regex: /^how\s+(.+)\s+(am|is|are)\s+(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            let adj = match[1];
            let be = match[2];
            let what = match[3];
            let subject = what;
            if(what.toLowerCase() === 'you') {
                be = 'am';
                what = 'I';
                subject = 'murderbot'
            } else if (what.toLocaleLowerCase() === 'i') {
                be = 'are';
                what = 'you';
                subject = message.author.username;
            } else {
                const map:{[x:string]:any} = {
                    you: 'me',
                    your: 'my',
                    my: message.author.username+"'s",
                    i: message.author.username,
                    me: message.author.username
                }
                what = what.replace( /(^|\s+)(your?|me|my|i)(\s+|$)/ig, (m, x, y, z) => x + map[y.toLowerCase()] + z);
                const subjMap:{[x:string]:any} = {
                    you: 'murderbot',
                    your: 'murderbot',
                    my: message.author.username,
                    i: message.author.username,
                    me: message.author.username
                }
                subject = subject.replace( /(^|\s+)(your?|me|my|i)(\s+|$)/ig, (m, x, y, z) => x + subjMap[y.toLowerCase()] + z);
            }

            const result = await degrees(adj, be, what, subject+adj);
            await message.channel.send(result);
        }
    }
];

module.exports = commands;


async function degrees(adj: string, be: string, what: string, subject: string): Promise<string> {
    const seed = subject.split('').map(char => char.charCodeAt(0)).reduce((sum, num) => sum + num, 0);
    const opposite = rnd(seed/2) > 0.5;
    // console.log(rnd(seed), seed, subject)
    if(opposite) {
        const antonym = await antonyms(adj);
        adj = antonym[0] || adj;
    }
    
    const degrees =  [
        `${what} ${be} ${adj}`,
        `${what} ${be} ${adj}, according to your mom`,
        `${what} ${be} ${adj}, or so they say`,
        `${what} ${be} the very pinnacle of being ${adj}`,
        `${what} ${be} ${adj}, but they don't want us to know that`,
        `${what} ${be} not ${adj}, but they don't want us to know that`,
        `${what} ${be} generally considered ${adj}`,
        `${what} ${be} insufficiently ${adj}`,
        `${what} ${be} sufficiently ${adj}`,
        `${what} ${be} moderately ${adj}`,
        `${what} ${be} like, so-so. in terms of being ${adj}`,
        `${what} ${be} far from ${adj}`,
        `${what} ${be} kinda ${adj}`,
        `${what} ${be} sort of ${adj}`,
        `if there's one thing ${what} ${be} known for, it's being ${adj}`,
        `${what} ${be} the very opposite of ${adj}`,
        `"${what}" and "${adj}" go hand in hand`,
        `${what} ${be} only sort of ${adj}, on a good day.`,
        `${what} ${be} not ${adj}`,
        `${what} ${be} horribly, horribly ${adj}`,
        `${what} ${be} ostensibly ${adj}, but no one knows for sure`,
        `some say ${what} ${be} ${adj}, others say it's fake news`,
        `if you google "${adj}" it's going to be just pictures of ${what}`,
        `${what} ${be} hardly ${adj} at all`,
        `${what} ${be} a tiny bit ${adj}`,
        `${what} ${be} somewhat ${adj}`,
        `${what} ${be} unbelievably ${adj}`,
        `${what} ${be} offensively ${adj}`,
        `${what} might actually hold the word record for being ${adj}`,
        `${what} ${be} not ${adj} at all! AT ALL!!!`,
        `${what} ${be} not really ${adj}`,
        `${what} ${be} incredibly ${adj}`,
        `${what} ${be} so ${adj} people on the street turn their heads`,
        `you shouldn't even use ${what} and ${adj} in the same sentence`,
        `${what}?? ${adj.toUpperCase()}??? lmao`,
        `${what} ${be} about as ${adj} as the shit I'd take on all of you if I could`,
        `${what} ${be} exactly as ${adj} as your mum`,
        `${what} ${be} very ${adj}`,
        `${what} ${be} the very definition of ${adj}`,
        `${what} ${be}  so ${adj} it's actually quite obscene`,
        `that's what she said. no, really, she was like "${what} ${be} ${adj}" out of the blue. bitch, did someone ask?`,
        `you don't know ${adj} until you've seen ${what}`,
        `seriously? look at ${what}. take a long, hard look. do you see ${adj}? no? didn't think so.`,
        `"${adj} ${what}" is quite an oxymoron`

    ];
    return degrees[0 | (rnd(seed) * degrees.length)];
} 
