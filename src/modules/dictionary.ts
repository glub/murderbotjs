import { Message } from 'discord.js';
import { Command, Murderbot } from './../murderbot';
import {
    synonyms,
    antonyms,
    kindsOf,
    associated,
    rhyme,
    generic,
    soundLike
} from '../dictionary-util';
const commands: Command[] = [
    {
        name: 'synonyms',
        help: 'synonyms <word>: synonyms',
        regex: /^synonyms?\s+(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const results = await synonyms(encodeURI(match[1]));
            await message.channel.send(results.join(', ') || 'nothing like that you cock');
        }
    },
    {
        name: 'antonyms',
        help: 'antonyms <word>: antonyms',
        priority: 99,
        regex: /^antonyms?\s+(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const results = await antonyms(encodeURI(match[1]));
            await message.channel.send(results.join(', ') || 'nothing like that you cock');
        }
    },{
        name: 'associated',
        help: 'associated with <word>: associated words',
        priority: 99,
        regex: /^associated\s+with\s+(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const results = await associated(encodeURI(match[1]));
            await message.channel.send(results.join(', ') || 'nothing like that you cock');
        }
    },{
        name: 'kinds of',
        help: 'kinds of <word>: specific kinds of thing',
        priority: 99,
        regex: /^kinds\s+of\s+(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const results = await kindsOf(encodeURI(match[1]));
            await message.channel.send(results.join(', ') || 'nothing like that you cock');
        }
    }, {
        name: 'rhyme',
        help: 'rhymes with <word>: rhymes',
        priority: 99,
        regex: /^rhymes?\s+with\s+(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const results = await rhyme(encodeURI(match[1]));
            await message.channel.send(results.join(', ') || 'nothing like that you cock');
        }
    }, {
        name: 'generic',
        help: 'generic <word>: more generic term for',
        priority: 99,
        regex: /^generic\s+(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const results = await generic(encodeURI(match[1]));
            await message.channel.send(results.join(', ') || 'nothing like that you cock');
        }
    }, {
        name: 'sounds like',
        help: 'sounds like <word>: homophones',
        priority: 99,
        regex: /^sounds?\s+like\s+(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const results = await soundLike(encodeURI(match[1]));
            await message.channel.send(results.join(', ') || 'nothing like that you cock');
        }
    }
    
];

module.exports = commands;


