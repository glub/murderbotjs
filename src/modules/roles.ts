import { ActionRowBuilder, AutocompleteInteraction, BitField, ChatInputCommandInteraction, ComponentType, GuildMember, GuildMemberRoleManager, MessageComponentInteraction, PermissionFlagsBits, PermissionsBitField, Role, RoleManager, SlashCommandBuilder, SlashCommandStringOption, StringSelectMenuBuilder, StringSelectMenuInteraction, StringSelectMenuOptionBuilder } from "discord.js";
import { Command } from "../murderbot";


const ADD_ROLE = 'add-role';

async function selectRoles(interaction: StringSelectMenuInteraction) {
    const member = interaction.member as GuildMember;
    const roles = interaction.component.options.map(o => o.value);
    const values = interaction.values;
    
    let toAdd = [];
    let toRemove = [];
    for (let i = 0, N = roles.length; i < N; i++) {
        let role = roles[i];
        if (values.indexOf(role) > -1) {
            toAdd.push(role);
        } else {
            toRemove.push(role);
        }
        
    }
    await member.roles.add(toAdd);
    await member.roles.remove(toRemove)
    
    
    await interaction.reply({
        ephemeral: true,
        content: 'roles updated'
    });

}


const commands: Command[] = [

    {
        name: 'add role',
        slashJSON: new SlashCommandBuilder()
                        .setName(ADD_ROLE)
                        .setDefaultMemberPermissions(PermissionFlagsBits.ManageRoles)
                        .setDescription('add a self-assign role')
                        .addStringOption( 
                            new SlashCommandStringOption()
                                .setName('category')
                                .setDescription('category to add the role to')
                                .setRequired(true)
                                .setAutocomplete(true)
                                
                            )
                        .addStringOption( 
                            new SlashCommandStringOption()
                                .setName('name')
                                .setDescription('role name')
                                .setRequired(true)
                                )

                        .toJSON(),
        slashName: ADD_ROLE,
        slashAutocomplete: async function(interaction: AutocompleteInteraction) {
            const focused = interaction.options.getFocused();

            const list = await this.allQuery(`select category from self_roles where guild = '${interaction.guild.id}'and  category like '%${focused}%'`)
            let  values = Array.from(new Set(list.map(el => el.category))) as string[];
            return values.map(val => ({name: val, value: val}));
        },
        slashRespond: async function(interaction: ChatInputCommandInteraction) {
            const name = interaction.options.get('name').value as string;
            const category = interaction.options.get('category').value;
            let role: Role;
            try {
                role = await interaction.guild.roles.create({
                    name
                });

            } catch (e) {
                interaction.reply({
                    ephemeral: true,
                    content: "whoops that didn't work"
                });
            }

            this.runQuery(`insert into self_roles values('${interaction.guild.id}','${name}','${role.id}','${category}')`)
        
            interaction.reply({
                content: 'role added',
                });
        },


        
        
        
    },
    {
        name: 'convert to self-role',
        slashName: 'convert-self-role',
        slashJSON: new SlashCommandBuilder()
                            .setName('convert-self-role')
                            .setDescription('make an existing role self-assignable')
                            .setDefaultMemberPermissions(PermissionFlagsBits.ManageRoles)
                            .addStringOption(
                                new SlashCommandStringOption()
                                    .setName('role')
                                    .setDescription('existing role name, non-case-sensitive')
                                    .setAutocomplete(true)
                                    .setRequired(true)
                            )
                            .addStringOption(
                                new SlashCommandStringOption()
                                    .setName('category')
                                    .setDescription('role category for grouping')
                                    .setAutocomplete(true)
                                    .setRequired(true)
                            )
                            .toJSON(),
        slashAutocomplete: async function (interaction: AutocompleteInteraction) {

            const focused = interaction.options.getFocused(true);
            if (focused.name === 'category') {
                const value = focused.value;
                const list = await this.allQuery(`select category from self_roles where guild = '${interaction.guild.id}' and category like '%${value}%'`)
                let  values = Array.from(new Set(list.map(el => el.category))) as string[];
                return values.map(val => ({name: val, value: val})); 

            } else if (focused.name === 'role') {
                const value = focused.value;
                const roles = Array.from(
                                interaction
                                    .guild
                                    .roles
                                    .cache
                                    .filter(r => r.name.toLowerCase().indexOf(value.toLowerCase()) > -1)
                                    .map(r => ({name: r.name, value: r.id}))
                                    .values())
                                    .slice(0,25);
                
                return roles;
            }
            return [];

        },
        slashRespond: async function (interaction: ChatInputCommandInteraction): Promise<void> {
            const id = interaction.options.get('role').value as string;
            const category = interaction.options.get('category').value as string;

            const {name} = await interaction.guild.roles.fetch(id);

            console.log(id, name, category);

            this.runQuery(`insert into self_roles values('${interaction.guild.id}','${name}','${id}','${category}')`)

            interaction.reply({
                content: 'role turned self-assigned:' + name,
                ephemeral: true
            });
            
            

        }
    },
    {
        name: 'roles post',
        slashName: 'role-select',
        slashJSON: new SlashCommandBuilder()
                        .setName('role-select')
                        .setDescription('open role selection dialog')
                        .addStringOption(
                            new SlashCommandStringOption()
                                .setName('category')
                                .setRequired(true)
                                .setAutocomplete(true)
                                .setDescription('only show roles of this category (optional)')
                        )
                        .toJSON(),
        slashAutocomplete: async function(interaction: AutocompleteInteraction) {
            const focused = interaction.options.getFocused();

            const list = await this.allQuery(`select category from self_roles where guild = '${interaction.guild.id}' and category like '%${focused}%'`)
            let  values = Array.from(new Set(list.map(el => el.category))) as string[];
            return values.map(val => ({name: val, value: val})); 
        },
        slashRespond: async function(interaction: ChatInputCommandInteraction) {

            let category = interaction.options.get('category').value as string;

            let roles = await this.allQuery(`select * from self_roles where guild = '${interaction.guild.id}' and category = '${category}'`);
            
            let existing = [];

            for (let i = 0, N = roles.length; i < N; i++) {
                let role = await interaction.guild.roles.fetch(roles[i].id);
                if (role) {
                    existing.push(roles[i]);
                } else {
                    this.runQuery(`delete from self_roles where id = '${roles[i].id}' and guild = '${interaction.guild.id}'`)
                    console.log('deleted role: ' + roles[i].name);
                }

            }

            roles = existing;


            if (roles.length === 0) {
                interaction.reply('no self-select roles defined on this server');
                return;
            }
            const sorted: {[x: string]: {
                    name: string;
                    id: string,
                    present: boolean
                }[]} = {};

                const options = roles.map(({name, id}) => {

                    let roles: GuildMemberRoleManager = interaction.member.roles as GuildMemberRoleManager;
                    const present = roles.cache.some(r => r.id === id);


                    return new StringSelectMenuOptionBuilder()
                                    .setLabel(name)
                                    .setValue(id)
                                    .setDefault(present)
                })
                const ui = new StringSelectMenuBuilder();
                ui.setPlaceholder('select roles');
                ui.setCustomId('mb_select_roles')
                ui.setOptions(options)
            
                let max = options.length;
                ui.setMaxValues(max);
                
                const bar: ActionRowBuilder<StringSelectMenuBuilder> = new ActionRowBuilder();
                bar.addComponents(ui);
                
                const response = await interaction.reply({
                    ephemeral: true,
                    content: category,
                    components: [bar]
                });
                    

                const collector = response.createMessageComponentCollector({
                    componentType: ComponentType.StringSelect, time: 3_600_000 
                });
                collector.on('collect', async i => {
                    await selectRoles(i);
                    interaction.deleteReply();
                });
        }
    }
];

module.exports = commands;
console.log(PermissionsBitField.All);