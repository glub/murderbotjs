import { Command } from "../murderbot";
import { Message } from "discord.js";
import { getPicURL } from "../util";
import { createCanvas, loadImage } from "canvas";

const ASCII = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\|()1{}[]?-_+~<>i!lI;:,\"^'";

const ADJUST = 0.45;

function sigmoid(x: number){
    return 1 / (1 + Math.exp((0.5-x)*16));
}

const commands: Command[] = [
    {
        name: 'ascii',
        regex: /^ascii( +\d+)?$/,
        help: 'ascii: turn last pic posted into ascii art',
        respond: async function(message: Message, match: RegExpMatchArray) {
            const link = await getPicURL(message);
            let size = parseInt(match[1]) || 66;
            // console.log(match[2], match,size)
            if(!link) {
                await message.channel.send("no pictures found bitch");
                return;
            }
            console.log("loading:", link);
            const img = await loadImage(link);
            const mult = Math.max(size/img.width, size/img.height);
            let w = Math.floor(img.width*mult);
            let h = Math.floor(img.height*mult*ADJUST);
            // console.log(w,h,w*h);
            if (w*h > 1980) {
                let down = Math.sqrt((w*h)/1980);
                w = Math.floor(w/down);
                h = Math.floor(h/down);
                // console.log(down);
            }
            console.log(w,h);


            const canvas = createCanvas(w,h);
            const ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0,w,h);
            const data = Array.from(ctx.getImageData(0,0,w,h).data);
            let result = '';
            for (let y = 0; y < h; y ++) {
                for (let x = 0; x < w; x ++) {
                    const index = 4*(y * w + x);
                    const [r,g,b,a] = data.slice(index,index+4);
                    let bright = (0.2126*r + 0.7152*g + 0.0722*b)*a;
                    bright /= 255*255;
                    bright = sigmoid(1 - bright);
                    // console.log(index, data.length, x, y);
                    result += ASCII[Math.floor(bright * ASCII.length)];
                }
                result += '\n';
            }

            message.channel.send('```\n' + result + '\n```');
            
        }

    }
];

module.exports = commands;