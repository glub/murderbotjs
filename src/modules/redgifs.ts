import { APIEmbed, Message } from 'discord.js';
import { promisify } from 'util';
import { choice } from '../util';
import { Command } from '../murderbot';
const fs = require('fs');
let token = null;
const MAX = 100;



enum GIF_API {
    SEARCH,
    TAG
};

function getUrl(type: GIF_API): (index: number, query: string) =>string {
    return {
        [GIF_API.SEARCH]: (page, query) => `https://api.redgifs.com/v2/search/gifs?page=${page}&count=${MAX}&order=top&query=${encodeURI(query)}&type=g&views=no`,
        [GIF_API.TAG]: (page, query) => `https://api.redgifs.com/v2/gifs/search?page=${page}&count=${MAX}&order=top&query=${encodeURI(query)}&type=g&views=no`
    }[type];
} 


async function fetchGif(index: number, query: string, type: GIF_API, all:boolean = false): Promise<string[]> {

    let page = 1;
    if(index > -1) {
        page = Math.floor(index / MAX) + 1;
    } else {
        index = -1;
    }
    
    if (!token) {
        token = await getToken();
    }


    const fetchUrl = getUrl(type)(page, query);
    
    const search = async () => fetch(
        fetchUrl,
        {
            method: 'GET',
            headers: {
                Authorization: token,
            }
        }
    );

    let response = await search();
   
    let json = await response.json();         
    

    if (json.error) {

        if (json.error.status === 401) {
            token = await getToken();
            response = await search();
            console.log("trying again");
            console.log("============================");
            json = await response.json()          
            console.log(json)

        }

        return ["Error: " + json.error.message];
    }

    const results = json.gifs;

    if (!results?.length) {
        return ['none of that you sick fuck'];
    }

    if (all) {
        let chunk = 5;
        return new Array(Math.ceil(MAX/chunk)).fill(0).map((_,i) => results.slice(i*chunk, (i+1)*chunk).map(e => e.urls.hd).join("\n"));
    }

    let entry;
    let entryIndex = index % MAX;

    if (index === -1) {
        entryIndex = Math.floor(Math.random() * results.length)
    }

    entry = results[entryIndex];

    // console.log(results.slice(0, 4));
    // console.log(entry);

    let msg = `${index === -1 ? entryIndex : index} out of ${json.total} \n`
    msg += entry.urls.hd;
    return [msg];
}


async function getToken(): Promise<string> {
    let response = await fetch(
        "https://api.redgifs.com/v2/auth/temporary"
    );

    let json = await response.json();
           
    console.log("requesting token...");
    console.log(json)
    console.log("====================================")
    return "Bearer "+json.token;

}

const commands: Command[] = [
    {
        name: 'redgif search',
        help: 'redgifs search <query> [#index]: reddit gifs search, highly nsfw',
        regex: /^redgifs search ([^#]+)( #(\d+))?$/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            
            
            let index = parseInt(match[3]);
            if (isNaN(index)) {
                index = -1;
            } else {
                index --;
            }
            const query = match[1];
            const msg= await fetchGif(index, query, GIF_API.SEARCH);
            for(let i = 0; i < msg.length; i++) {
                await message.channel.send(msg[i]);
            }

        }
    },
    {
        name: 'redgif search tag',
        help: 'redgifs tag <query> [#index]: reddit gifs search by tags, highly nsfw',
        regex: /^redgifs tag ([^#]+)( #(\d+))?$/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            
            
            let index = parseInt(match[3]);
            if (isNaN(index)) {
                index = -1;
            } else {
                index --;
            }
            const query = match[1];
            const msg= await fetchGif(index, query, GIF_API.SEARCH);

            for(let i = 0; i < msg.length; i++) {
                await message.channel.send(msg[i]);
            }

        }
    },
    {
        name: 'redgif search all',
        regex: /^redgifs searchall ([^#]+)( #(\d+))?$/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            
            
            let index = parseInt(match[3]);
            if (isNaN(index)) {
                index = -1;
            } else {
                index --;
            }
            const query = match[1];
            const msg= await fetchGif(index, query, GIF_API.SEARCH, true);
            for(let i = 0; i < msg.length; i++) {
                await message.channel.send(msg[i]);
            }

        }
    },
    {
        name: 'redgif search tag',
        regex: /^redgifs tagall ([^#]+)( #(\d+))?$/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            
            
            let index = parseInt(match[3]);
            if (isNaN(index)) {
                index = -1;
            } else {
                index --;
            }
            const query = match[1];
            const msg= await fetchGif(index, query, GIF_API.SEARCH, true);

            for(let i = 0; i < msg.length; i++) {
                await message.channel.send(msg[i]);
            }

        }
    },
    {
        name: 'redgif tags',
        help: 'redgifs tags <>: redgif tag list',
        regex: /^redgifs tags (.+)$/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            
            const query = match[1]

            if (!token) {
                token = await getToken();
            }
            const url =   `https://api.redgifs.com/v2/tags/suggest?query=${query}&count=20`
            const search = async () => fetch(
                url,
                {
                    method: 'GET',
                    headers: {
                        Authorization: token,
                    }
                }
            );
        
            let response = await search();
           
            let json = await response.json();         
            
        
            if (json.error) {
        
                if (json.error.status === 401) {
                    token = await getToken();
                    response = await search();
                    console.log("trying again");
                    console.log("============================");
                    json = await response.json()          
                    console.log(json)
        
                }
        
                message.channel.send("Error: " + json.error.message);
            }
            console.log(json);
            let msg = '```\n' + json.join('\n') +'\n```';
        
            message.channel.send(msg);

        }
    }
];
module.exports = commands;