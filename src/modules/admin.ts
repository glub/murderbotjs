import { Command } from "../murderbot";
import { Message, MessageReaction } from "discord.js";
const LIMIT: number = 1900;
import * as util from 'util';
const commands: Command[] = [
    // {
    //     name: 'delet this',
    //     reactIf: async function(reaction: MessageReaction) {
    //         return reaction.message.author.id === this.client.user.id
    //                 && reaction.emoji.toString() === '❌' 
    //                 && reaction.users.has(this.adminID);
    //     },
    //     react: async function(reaction: MessageReaction) {
    //         await reaction.message.delete();
    //     }
    // },
    {
        name: 'logs',
        admin: true,
        regex: /^logs?( +(\d+))?/i,
        respond: async function (message: Message, match: RegExpMatchArray): Promise<void> {
            const limit = match[2];
            const logs = await this.allQuery(`select * from log` + limit ? ` limit ${limit}`: '');
            message.channel.send(logs);
        }
    },{
        name: 'query run',
        admin: true,
        regex: /^query run +((.|\n)+)/im,
        respond: async function(message: Message, match: RegExpMatchArray): Promise<void> {
            await this.runQuery(match[1]);
            message.channel.send('done')
        }
    
    },{
        name: 'query get',
        admin: true,
        regex: /^query get +((.|\n)+)/im,
        respond: async function(message: Message, match: RegExpMatchArray): Promise<void> {
            const result = await this.getQuery(match[1]);
            message.channel.send(Object.keys(result).map(k => `${k}: ${result[k]}`).join('\n').slice(0, LIMIT));
        }
    
    },{
        name: 'query all',
        admin: true,
        regex: /^query all +((.|\n)+)/im,
        respond: async function(message: Message, match: RegExpMatchArray): Promise<void> {
            const result = await this.allQuery(match[1]);
            console.log(result);
            message.channel.send(result.map(el => Object.values(el).join(' ')).join('\n').slice(0, LIMIT) || 'Empty');
        }
    }, {
        name: 'eval',
        admin: true,
        regex: /^eval *?\n?```.*\n((.|\n)+)\n```/im,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            let result = match[1];
            console.log(result);
            try {
                result = util.inspect(eval(result));
                
            }catch(e){
                result = e;
            }
            if(!result) {
                result = 'undefined';
            }
            if(result.length > LIMIT) {
                result = result.slice(0, LIMIT) + '\n...';
            }
            await message.channel.send({content: '```js\n' + result + '\n```'});
        }
    }, {
        name: 'delete',
        admin: true,
        reactIf: async function(reaction: MessageReaction): Promise<boolean> {

            console.log(reaction.message.author.client.user.id, this.client.user.id)
            console.log(reaction.message.author.client.user.username, this.client.name)
            return reaction.emoji.name === '❌' && reaction.message.author.client.user.id === this.client.user.id;
        },
        react: async function(reaction: MessageReaction) {
            reaction.message.delete();
        }

    }
];

module.exports = commands;