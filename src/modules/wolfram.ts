import { Message } from 'discord.js';
import { Command, Murderbot } from './../murderbot';
import { promisify } from 'util';
const request = promisify(require('request'));
const keys = require('../../keys.js');


const commands: Command[] = [
    {
        name: 'wolfram',
        help: 'tell me <question>: Wolfram|Alpha short answer API',
        regex: /^tell\s+me\s+(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const question = match[1];
            const url = `http://api.wolframalpha.com/v1/result?i=${encodeURI(question)}&appid=${keys.wolfram}`;
            const response = await request(url);
            await message.channel.send(response.body);
        }
    }

];
module.exports = commands;