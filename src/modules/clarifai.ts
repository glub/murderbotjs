import { Message } from 'discord.js';
import { Command, Murderbot } from './../murderbot';
import { getPicURL } from '../util';
const {ClarifaiStub, grpc} = require('clarifai-nodejs-grpc');
const stub = ClarifaiStub.grpc();
const meta = new grpc.Metadata();
const keys = require('../../keys.js');
meta.set("authorization", "Key " + keys.clarifai);

const models = {
    nsfw : "e9576d86d2004ed1a38ba0cf39ecb4b1",
    celebrities : "celebrity-face-recognition",
    // demographics : "c0c0ac362b03416da06ab3fa36fb58e3",
    ethnicity: "ethnicity-demographics-recognition",
    gender: "gender-demographics-recognition",
    age: "age-demographics-recognition",
    general: "aaa03c23b3724a16a56b629203edc62c",
}

const LEWDOMETER = [
    "Pure and Innocent",
    "safe",
    "safe... I think...",
    "t-that might be a little lewd",
    "that's lewd! probably",
    "that's definitely kinda lewd!",
    "lewd!",
    "very lewd!",
    "very very lewd!!",
    "lewd! LEWD!!:scream:"
];

const NOPIC = 'attach a file or link something you nincompoop';


const commands: Command[] = [
    {
        name: 'who dis',
        help: 'who is this (that) <link or attachment or reply to a message with a pic or do none of these then the last picture posted in the channel will be used>: mug  recognition',
        regex: /^who\s+is\s+(this|that) *(\s+(\S+))?/,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const link = await getPicURL(message) || match[2];
            if(!link) {
                await message.channel.send(NOPIC);
                return;
            }
            try {
                const celebrity = await getCeleb(link);
                const age = await getAge(link);
                const ethnicity = await getEthnicity(link);
                const gender = await getGender(link);

                message.channel.send(`${age} ${ethnicity}  ${gender} that looks like ${celebrity}`);

            } catch (e) {
                console.log(e);
                await message.channel.send('something went wrong. probably your fault');
            }
        }
    }, {
        name: 'general',
        help: 'what is this (that) <link or attachment or reply to a message with a pic or do none of these then the last picture posted in the channel will be used>: tagging',
        regex: /^what\s+is\s+(this|that)(\s+(\S+))?/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const link = await getPicURL(message) || match[2];
            if(!link) {
                await message.channel.send(NOPIC);
                return;
            }
            try {
                const result = await general(link);
                await message.channel.send(result);
            } catch(e) {
                message.channel.send('something broke');
            }
  
        }
    }, {
        name: 'nsfw',
        help: 'nsfw <link or attachment or reply to a message with a pic or do none of these then the last picture posted in the channel will be used>: gauge the lewdness',
        regex: /^n?sfw(\s+(\S+))?/,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            const link = match[2] || await getPicURL(message);
            if(!link) {
                await message.channel.send(NOPIC);
                return;
            }
            try {
                const result = await nsfw(link);
                await message.channel.send(LEWDOMETER[result])

            } catch (e) {
                message.channel.send('something broke');
            }
        }
    }

];
module.exports = commands;


async function predict(model: string, what: string): Promise<any> {

    return new Promise(((resolve, reject) => {
        stub.PostModelOutputs(
            {
                model_id: model,
                inputs: [{data: {image: {url: what}}}]
            }, meta,
            (err, res) => {
                if (res) {
                    resolve(res.outputs[0].data)
                } else {
                    reject(err);
                }
            });  
    }));
}

async function nsfw(link: string): Promise<number> {
    const result = await predict(models.nsfw, link);
    const value = result
        .concepts
        .filter((concept: any) => concept.name === 'nsfw')[0]
        .value;
    return (value * 10) | 0;
}

async function getCeleb(link: string): Promise<string> {
    const PROB = v => {
        if (v < 0.1) {
            return 'a little bit';
        } else if (v < 0.3) {
            return 'sort of'
        } else if ( v < 0.6) {
            return 'quite a bit';
        } else if (v < 0.8) {
            return 'quite a lot!';
        } else {
            return "so much it's uncanny"
        }
    }
    const result = (await predict(models.celebrities, link)).concepts[0];
    return `${result.name} (${PROB(result.value)})`;
}




async function general(link: string): Promise<string> {
    const result = await predict(models.general, link);
    return result.concepts
        .map((concept: any) => concept.name).join(', ');
}




async function getAge(link: string): Promise<string> {
    const result = (await predict(models.age, link)).concepts[0];
    return result.name + 'yo';

}
async function getGender(link: string): Promise<string> {
    const result = (await predict(models.gender, link)).concepts[0].name;
    return result.toLowerCase().startsWith('m') ? "bloke" : "lady";
}
async function getEthnicity(link: string): Promise<string> {
    const PROB = v => {
        if (v < 0.1) {
            return '...maybe';
        } else if (v < 0.3) {
            return 'probably'
        } else if ( v < 0.6) {
            return 'most likely';
        } else if (v < 0.8) {
            return 'for sure';
        } else {
            return 'definitely'
        }
    }
      
    const result = (await predict(models.ethnicity, link)).concepts[0];

    return `${result.name.toLowerCase()} (${PROB(result.value)})`;
}
