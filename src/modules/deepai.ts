import { Message } from 'discord.js';
import { Command, Murderbot } from './../murderbot';
const deepai = require('deepai');
const keys = require('../../keys.js');
const commands: Command[] = [
    {
        name: 'text 2 image',
        help: 'draw [description]',
        regex: /^draw\s+(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            console.log(match[1]);
            deepai.setApiKey(keys.deepai);
            var resp = await deepai.callStandardApi("text2img", {
                text: match[1],
            });
            await message.channel.send(match[1] + ':');
            await message.channel.send(resp.output_url);
        }
    },
    {
        name: 'text generator',
        help: 'finish [text]',
        regex: /^finish\s+(.+)/i,
        respond: async function(message: Message, match: RegExpMatchArray)  {
            console.log(match[1]);
            deepai.setApiKey(keys.deepai);
            var resp = await deepai.callStandardApi("text-generator", {
                text: match[1],
            });
            console.log(resp);
            await message.channel.send(resp.output.slice(0, 1900));
        },
    },
    // {
    //     name: 'stylize',
    //     help: 'stylize - will use the last two images in the channel',
    //     regex: /^stylize/i,
    //     respond: async function(message: Message, match: RegExpMatchArray)  {
    //         console.log(match[1]);
    //         deepai.setApiKey(keys.deepai);
    //         var resp = await deepai.callStandardApi("text-generator", {
    //             style: 'https://api.deepai.org/job-view-file/4ad2484e-bcf6-45b2-96ca-e099d25808b4/outputs/output.jpg',
    //             content: 'https://images.deepai.org/machine-learning-models/f7ac838cec1045698b818582b2412d56/neural-style.jpg'
    //         });
    //         console.log(resp);
    //         await message.channel.send(resp.output_url);
    //     },
    // },
];
module.exports = commands;