import { Message, ReactionCollector } from 'discord.js';
import { Command, Murderbot } from './../murderbot';

const EMOJI = [
    '🇦',
    '🇧',
    '🇨',
    '🇩',
    '🇪',
    '🇫',
    '🇬',
    '🇭',
    '🇮',
    '🇯',
    '🇰',
    '🇱',
    '🇲',
    '🇳',
    '🇴',
    '🇵',
    '🇶',
    '🇷',
    '🇸',
    '🇹',
    '🇺',
    '🇻',
    '🇼',
    '🇽',
    '🇾',
    '🇿'
];
const LETTERS = [
    'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'
];

const Commands: Command[] = [
    // {
    //     name: 'poll',
    //     help: 'poll <question>? [answer 1], [answer 2], ... : poll with emoji',
    //     regex: /^poll\s+(.+)\?\s*(.+)?/i,
    //     respond: async function(message: Message, match: RegExpMatchArray)  {
    //         const question = '# ' + match[1];
    //         const options = (match[2] || 'yes, no, maybe').split(/\s*,\s*/).slice(0, 26);
    //         const post = pollMessage(question, options);
    //         const poll = (<Message>await message.channel.send(post));
    //         for(let i = 0; i < options.length; i++) {
    //             await poll.react(EMOJI[i]);
    //         }
    //         const collector = new ReactionCollector(poll, rection => EMOJI.indexOf(rection.emoji.name) !== -1);

    //         let winning = 1;
    //         let votes: number[] = [];
    //         collector.on('collect', reaction => {
    //             winning = 0;
    //             poll.reactions..forEach((r, i) => {
    //                 let size = r.users.size;
    //                 votes[EMOJI.indexOf(r.emoji.name)] = size;
    //                 if(size > winning) {    
    //                     winning = size;
    //                 }});
    //             poll.edit(pollMessage(question, options, winning, votes));
    //         });
    //         setTimeout(_ => {
    //             collector.stop();
    //             poll.edit(pollMessage(question, options, winning, votes) + '\n THE DEAL IS SEALED');
    //         }, 600000);



    //     }
    // }
];


function pollMessage(question: string, options: string[], win: number = 0, votes: number[]= []): string {
    return `
    \`\`\`csharp
${question}

${options.map((o, i) => 
    (votes[i] === win ? '# ' : 'x ') + LETTERS[i] + ' ' + o
).join('\n')}
    \`\`\`
    `;
}

module.exports = Commands;
