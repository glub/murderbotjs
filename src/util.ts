import { Murderbot } from './murderbot';
import { Message } from 'discord.js';
import { inspect, promisify } from 'util';
import { writeFile } from 'fs';
import { resolve } from 'path';
const writeFileAsync = promisify(writeFile);

const request = promisify(require('request'));

export async function getPicURL(message: Message): Promise<null|string> {

    
    if (message.reference) {
        const resolvable = await message.channel.messages.fetch(message.reference.messageId);
        const answerTo = message.channel.messages.resolve(resolvable);
        const pic = getPicURLSingle(answerTo);
        console.log(answerTo);
        if (pic) {
            return pic;
        } else {
            message.channel.send('no pic in that message you replied to numbnuts');
            return null;
        }
    }

    const current = getPicURLSingle(message);
    if (current) {
        return current;
    }
    const messages:any = await message.channel.messages.fetch({limit: 100, before: message.id});
    const prev:any = Array.from(messages.values()); 
    prev.sort((a,b) => b.createdTimestamp - a.createdTimestamp);
    
    for (let i = 0; i < prev.length; i++) {
        const url = getPicURLSingle(prev[i]);
        if (url) {
            return url;
        }
    }
    // console.log()a
    return null;
}
export function getPicURLSingle(message: Message): null|string {

    const embed = message.embeds.find(e => e.data.thumbnail || e.data.image);
    // console.log(message);
    if(embed) {
        console.log("found embed", (embed.data.thumbnail || embed.data.image).url);
        return (embed.data.thumbnail || embed.data.image).url;
    }
    const attachment = message.attachments.first();
    
    if(attachment && /\.(jpe?g|gif|png|)$/.test(attachment.url)) {
        console.log("found attachment", attachment.url);
        return attachment.url;
    }

    const urlmatch = message.content.match(/https?\S+/);
    if (urlmatch && urlmatch[1]) {
        return urlmatch[1];
    }
    return null;
}




export async function download(url: string, name: string, folder: string): Promise<any> {
    folder = folder || '';
    if(!name) {
        let levels = url.split('/');
        name = levels[levels.length - 1];
        name = name.replace(/\?.+$/, '');
    }
    const filepath = resolve(folder, name);
    const dl = await request({url, encoding: null});
    await writeFileAsync(filepath, dl.body);
}


export function escapeRegExp(str: string): string {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

export function choice<T>(array: T[]): T {
    return array[0 | (array.length * Math.random())];
}

export function shuffle<T>(array: T[]): T[] {
    const src = [];
    while(array.length > 0) {
        let rnd = 0 | (Math.random() * array.length);
        let el =  array.splice(rnd, 1)[0];
        src.push(el);
    }
    return src;
}

export function rnd(seed: number): number {
    return Math.abs(Math.sin(seed*76.233) * 43758.5453) % 1;
}