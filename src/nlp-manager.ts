import {NlpManager} from 'node-nlp';

export const manager = new NlpManager({ languages: ['en'], forceNER: true });

